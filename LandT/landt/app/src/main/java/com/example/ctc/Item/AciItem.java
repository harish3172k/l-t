package com.example.ctc.Item;

public class AciItem {


    int aciID, jobCode2, userId2, jobid2, duration2, role, jobcode;
    String jobname2, joblocation2, nameofstructure2, nameofelement2, name2, branch2, username2, formulaVersion2, elementType2, cementType2, internalVibration2, createdon2, slag2, flyash2, retarders2, slump2, wallheight2, latitude2, cluster2, longtiude2;
    double den2, volume2, verticalformheight2, planareas2, temperatureee2, pmax2;


    public AciItem(int aciID, int jobCode2, int userId2, int jobid2, int duration2, int role, int jobcode, String jobname2, String joblocation2, String nameofstructure2, String nameofelement2, String name2, String branch2, String username2, String formulaVersion2, String elementType2, String cementType2, String internalVibration2, String createdon2, String slag2, String flyash2, String retarders2, String slump2, String wallheight2, String latitude2, String cluster2, String longtiude2, double den2, double volume2, double verticalformheight2, double planareas2, double temperatureee2, double pmax2) {
        this.aciID = aciID;
        this.jobCode2 = jobCode2;
        this.userId2 = userId2;
        this.jobid2 = jobid2;
        this.duration2 = duration2;
        this.role = role;
        this.jobcode = jobcode;
        this.jobname2 = jobname2;
        this.joblocation2 = joblocation2;
        this.nameofstructure2 = nameofstructure2;
        this.nameofelement2 = nameofelement2;
        this.name2 = name2;
        this.branch2 = branch2;
        this.username2 = username2;
        this.formulaVersion2 = formulaVersion2;
        this.elementType2 = elementType2;
        this.cementType2 = cementType2;
        this.internalVibration2 = internalVibration2;
        this.createdon2 = createdon2;
        this.slag2 = slag2;
        this.flyash2 = flyash2;
        this.retarders2 = retarders2;
        this.slump2 = slump2;
        this.wallheight2 = wallheight2;
        this.latitude2 = latitude2;
        this.cluster2 = cluster2;
        this.longtiude2 = longtiude2;
        this.den2 = den2;
        this.volume2 = volume2;
        this.verticalformheight2 = verticalformheight2;
        this.planareas2 = planareas2;
        this.temperatureee2 = temperatureee2;
        this.pmax2 = pmax2;
    }

    public AciItem() {


    }


    public int getAciID() {
        return aciID;
    }

    public void setAciID(int aciID) {
        this.aciID = aciID;
    }

    public int getJobCode2() {
        return jobCode2;
    }

    public void setJobCode2(int jobCode2) {
        this.jobCode2 = jobCode2;
    }

    public int getUserId2() {
        return userId2;
    }

    public void setUserId2(int userId2) {
        this.userId2 = userId2;
    }

    public int getJobid2() {
        return jobid2;
    }

    public void setJobid2(int jobid2) {
        this.jobid2 = jobid2;
    }

    public int getDuration2() {
        return duration2;
    }

    public void setDuration2(int duration2) {
        this.duration2 = duration2;
    }

    public int getRole() {
        return role;
    }

    public void setRole(int role) {
        this.role = role;
    }

    public int getJobcode() {
        return jobcode;
    }

    public void setJobcode(int jobcode) {
        this.jobcode = jobcode;
    }

    public String getJobname2() {
        return jobname2;
    }

    public void setJobname2(String jobname2) {
        this.jobname2 = jobname2;
    }

    public String getJoblocation2() {
        return joblocation2;
    }

    public void setJoblocation2(String joblocation2) {
        this.joblocation2 = joblocation2;
    }

    public String getNameofstructure2() {
        return nameofstructure2;
    }

    public void setNameofstructure2(String nameofstructure2) {
        this.nameofstructure2 = nameofstructure2;
    }

    public String getNameofelement2() {
        return nameofelement2;
    }

    public void setNameofelement2(String nameofelement2) {
        this.nameofelement2 = nameofelement2;
    }

    public String getName2() {
        return name2;
    }

    public void setName2(String name2) {
        this.name2 = name2;
    }

    public String getBranch2() {
        return branch2;
    }

    public void setBranch2(String branch2) {
        this.branch2 = branch2;
    }

    public String getUsername2() {
        return username2;
    }

    public void setUsername2(String username2) {
        this.username2 = username2;
    }

    public String getFormulaVersion2() {
        return formulaVersion2;
    }

    public void setFormulaVersion2(String formulaVersion2) {
        this.formulaVersion2 = formulaVersion2;
    }

    public String getElementType2() {
        return elementType2;
    }

    public void setElementType2(String elementType2) {
        this.elementType2 = elementType2;
    }

    public String getCementType2() {
        return cementType2;
    }

    public void setCementType2(String cementType2) {
        this.cementType2 = cementType2;
    }

    public String getInternalVibration2() {
        return internalVibration2;
    }

    public void setInternalVibration2(String internalVibration2) {
        this.internalVibration2 = internalVibration2;
    }

    public String getCreatedon2() {
        return createdon2;
    }

    public void setCreatedon2(String createdon2) {
        this.createdon2 = createdon2;
    }

    public String getSlag2() {
        return slag2;
    }

    public void setSlag2(String slag2) {
        this.slag2 = slag2;
    }

    public String getFlyash2() {
        return flyash2;
    }

    public void setFlyash2(String flyash2) {
        this.flyash2 = flyash2;
    }

    public String getRetarders2() {
        return retarders2;
    }

    public void setRetarders2(String retarders2) {
        this.retarders2 = retarders2;
    }

    public String getSlump2() {
        return slump2;
    }

    public void setSlump2(String slump2) {
        this.slump2 = slump2;
    }

    public String getWallheight2() {
        return wallheight2;
    }

    public void setWallheight2(String wallheight2) {
        this.wallheight2 = wallheight2;
    }

    public String getLatitude2() {
        return latitude2;
    }

    public void setLatitude2(String latitude2) {
        this.latitude2 = latitude2;
    }

    public String getCluster2() {
        return cluster2;
    }

    public void setCluster2(String cluster2) {
        this.cluster2 = cluster2;
    }

    public String getLongtiude2() {
        return longtiude2;
    }

    public void setLongtiude2(String longtiude2) {
        this.longtiude2 = longtiude2;
    }

    public double getDen2() {
        return den2;
    }

    public void setDen2(double den2) {
        this.den2 = den2;
    }

    public double getVolume2() {
        return volume2;
    }

    public void setVolume2(double volume2) {
        this.volume2 = volume2;
    }

    public double getVerticalformheight2() {
        return verticalformheight2;
    }

    public void setVerticalformheight2(double verticalformheight2) {
        this.verticalformheight2 = verticalformheight2;
    }

    public double getPlanareas2() {
        return planareas2;
    }

    public void setPlanareas2(double planareas2) {
        this.planareas2 = planareas2;
    }

    public double getTemperatureee2() {
        return temperatureee2;
    }

    public void setTemperatureee2(double temperatureee2) {
        this.temperatureee2 = temperatureee2;
    }

    public double getPmax2() {
        return pmax2;
    }

    public void setPmax2(double pmax2) {
        this.pmax2 = pmax2;
    }
}