package com.example.ctc.Activity;

import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.ctc.CiriaData;
import com.example.ctc.Response.CiriaResponse;
import com.example.ctc.R;
import com.example.ctc.Result.ResultCIRIA;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.entity.StringEntity;

public class CiriaActivity extends AppCompatActivity {

   Button cal,clr,vr;
   Spinner rolec1,rolec2;
   TextInputEditText den,ph,fh,vol,rate,temperatue;
   TextView pm,tem1;
   double C1,C2;
   TextInputLayout textInputLayout3;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ciria_temp);
        final SharedPreferences preferences =getSharedPreferences("checkbox",0);

        cal = findViewById(R.id.button5);
        clr = findViewById(R.id.button9);
        tem1=findViewById(R.id.textView18);
        vr = findViewById(R.id.button8);
        rolec1=findViewById(R.id.C1);
        rolec2=findViewById(R.id.C2);
        den= findViewById(R.id.density);
        ph= findViewById(R.id.pourheight);
        fh= findViewById(R.id.formheight);
        vol= findViewById(R.id.volume);
        rate= findViewById(R.id.planarea);
        pm=findViewById(R.id.pmax);
        temperatue=findViewById(R.id.temp);
        //temperatue.setText(preferences.getString("Temperature","0.0"));
        tem1.setText("Atmospheric Temperature : " +preferences.getString("Temperature","0.0") + "°C");

        textInputLayout3=findViewById(R.id.textInputLayout3);






        // Toast.makeText(getApplicationContext(), "Latitude: " + preferences.getString("Latitude", ""), Toast.LENGTH_LONG).show();

        clr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                den.getText().clear();
                fh.getText().clear();
                ph.getText().clear();
                vol.getText().clear();
                rate.getText().clear();
                pm.setText("PMAX");
                temperatue.getText().clear();
                ArrayAdapter<String> c1 = new ArrayAdapter<String>(CiriaActivity.this, R.layout.support_simple_spinner_dropdown_item, getResources().getStringArray(R.array.elementciria));
                c1.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
                rolec1.setAdapter(c1);
                ArrayAdapter<String> c2 = new ArrayAdapter<String>(CiriaActivity.this, R.layout.support_simple_spinner_dropdown_item, getResources().getStringArray(R.array.c2ciria));
                c2.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
                rolec2.setAdapter(c2);




            }
        });
        ArrayAdapter<String> c1 = new ArrayAdapter<String>(CiriaActivity.this, R.layout.support_simple_spinner_dropdown_item, getResources().getStringArray(R.array.elementciria));
        c1.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
        rolec1.setAdapter(c1);

        rolec1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                String text = rolec1.getSelectedItem().toString();
                switch (text) {
                    case "Walls":
                        C1 =1.0;
                        break;
                    case "Columns":
                        C1 =1.5;
                        break;


                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                Toast.makeText(CiriaActivity.this, "Please Select C1", Toast.LENGTH_SHORT).show();
            }
        });
        ArrayAdapter<String> c2 = new ArrayAdapter<String>(CiriaActivity.this, R.layout.support_simple_spinner_dropdown_item, getResources().getStringArray(R.array.c2ciria));
        c2.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
        rolec2.setAdapter(c2);

        rolec2.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                String text = rolec2.getSelectedItem().toString();
                switch (text) {
                    case "OPC,RHPC or SRPC without admixtures":
                        C2 = 0.3;
                        break;
                    case "OPC,RHPC or SRPC with any admixtures,except a retarder":
                        C2 = 0.3;
                        break;
                    case "OPC,RHPC or SRPC with a reterder":
                        C2 = 0.45;
                        break;
                    case "LHBFC,PBFC,PPFAC or blends containing 70%ggbfs or 40%pfa without admixtures":
                        C2 = 0.45;
                        break;
                    case "LHBFC,PBFC,PPFAC or blends containing lesd than 70% ggbfs or 40% pfa with any admixtures,except a retarder":
                        C2 = 0.45;
                        break;
                    case "LHPBC,PBFC,PPFAC or blends containing 70% ggbfs or 40% pfs with a retarder":
                        C2 = 0.6;
                        break;
                    case "Blends containing more than 70% ggbfs or 40%pfa":
                        C2 = 0.6;
                        break;

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                Toast.makeText(CiriaActivity.this, "Please Select C2", Toast.LENGTH_SHORT).show();
            }
        });



        cal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final Animation animation = AnimationUtils.loadAnimation(CiriaActivity.this,R.anim.bounce);
                cal.startAnimation(animation);

                // Toast.makeText(getApplicationContext(),"Calculate Pressed",Toast.LENGTH_LONG).show();

                if (den.getText().length() == 0)
                {

                    den.setError("Density Required");

                }
                else if(ph.getText().length() == 0)
                {

                    ph.setError("Pouring Height Required");
                }
                else if(fh.getText().length()==0)
                {
                    fh.setError("Form Height Required");
                }
                else if(vol.getText().length() == 0)
                {
                    vol.setError("Volume Required");
                }
                else if(rate.getText().length() == 0)
                {
                    rate.setError("Rate Of Placement Required");
                }
                else if(temperatue.getText().length()==0)
                {
                    temperatue.setError("Temperature Required");
                }
                else

                    {
                    // String den= density.getText().toString();

                    final double c2 = C2;
                    final double elementt = C1;
                    final double Den =Double.parseDouble(den.getText().toString());
                    final double a = Double.parseDouble(fh.getText().toString());
                    final double b = Double.parseDouble(ph.getText().toString());
                    final double c = Double.parseDouble(vol.getText().toString());
                    final double t=Double.parseDouble(temperatue.getText().toString());
                    //final double d = Double.parseDouble(contemp.getText().toString());
                    final double e2 = Double.parseDouble(rate.getText().toString());
                    //final double r=Double.parseDouble(wallrop1.getText().toString());


                    AsyncHttpClient client = new AsyncHttpClient();
                    JSONObject jsonObject = new JSONObject();
                    StringEntity entity = null;

                    CiriaData ciriaData = new CiriaData(
                            c2,
                            elementt,
                            Den,
                            a,
                            b,
                            c,
                            e2
                    );
                    SharedPreferences.Editor editor = preferences.edit();
                    editor.putString("c1",String.valueOf(ciriaData.getElementt()));
                    editor.putString("c2",String.valueOf(ciriaData.getC2()));
                    editor.putString("Density",String.valueOf(ciriaData.getDensity()));
                    editor.putString("Form_Height",String.valueOf(ciriaData.getVerticalFormHeight()));
                    editor.putString("Pour_Height",String.valueOf(ciriaData.getVerticalPourHeight()));
                    editor.putString("Volume",String.valueOf(ciriaData.getVolume()));
                    editor.putString("Rate",String.valueOf(ciriaData.getRop()));
                    editor.putString("Temperature1",temperatue.getText().toString());

                    editor.apply();


                    try {
                        entity = new StringEntity("{}");
                        jsonObject.put("jobID",preferences.getInt("Jobid",0));
                        jsonObject.put("userId", preferences.getInt("userID", 0));
                        jsonObject.put("latitude", preferences.getString("Latitude", null));
                        jsonObject.put("longtitude", preferences.getString("Longitude", null));
                        jsonObject.put("elementt", ciriaData.getElementt());
                        jsonObject.put("c2", ciriaData.getC2());
                        jsonObject.put("density", ciriaData.getDensity());
                        jsonObject.put("verticalFormHeight", ciriaData.getVerticalFormHeight());
                        jsonObject.put("verticalPourHeight", ciriaData.getVerticalPourHeight());
                        jsonObject.put("volume", ciriaData.getVolume());
                        jsonObject.put("rate", ciriaData.getRop());
                        jsonObject.put("temperature",Double.valueOf(preferences.getString("Temperature1","")));
                        entity = new StringEntity(jsonObject.toString());

                    } catch (UnsupportedEncodingException e1) {
                        e1.printStackTrace();
                    } catch (JSONException e1) {
                        e1.printStackTrace();
                    }
                    client.post(CiriaActivity.this, "http://rmkeclandt-env.miurmysbmy.us-east-2.elasticbeanstalk.com/ciria", entity, "application/json", new AsyncHttpResponseHandler() {
                        @Override
                        public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                            try {
                                Log.d("Demo Success: ", new JSONObject(new String(responseBody, "UTF-8")).toString());
                                JSONObject jsonObject = new JSONObject(new String(responseBody, "UTF-8"));
                                CiriaResponse ciriaResponse = new CiriaResponse(jsonObject.getDouble("pmax"));
                                SharedPreferences.Editor editor = preferences.edit();
                                editor.putInt("Ciria_id", jsonObject.getInt("ciriaID"));
                                editor.putString("Temperature1",String.valueOf(jsonObject.getDouble("temperature")));
                                editor.putString("Pmax",String.valueOf(jsonObject.getDouble("pmax")));
                                editor.apply();
                                Log.d("Demo pMax: ", ciriaResponse.toString());
                                Toast.makeText(CiriaActivity.this, "Result calculated successfully", Toast.LENGTH_SHORT).show();
                                pm.setText(Double.toString(ciriaResponse.getPmax()));
                            } catch (JSONException e1) {
                                e1.printStackTrace();
                            } catch (UnsupportedEncodingException e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                            try {
                                JSONObject jsonObject = new JSONObject(new String(responseBody, "UTF-8"));
                                Toast.makeText(getApplicationContext(), jsonObject.getString("message"), Toast.LENGTH_LONG).show();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            } catch (UnsupportedEncodingException e) {
                                e.printStackTrace();
                            }
                        }
                    });
                }
            }
        });


        vr.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                Intent intent = new Intent(CiriaActivity.this, ResultCIRIA.class);
                startActivity(intent);
            }
        });




    }



}

