package com.example.ctc;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import  android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;
import android.widget.Toast;

import com.example.ctc.Activity.LoginActivity;
import com.example.ctc.Beans.DashboardActivity;

import java.util.Locale;

public class Splash_temp extends AppCompatActivity implements LocationListener {

    TextView txt;
    double latitude, longitude;
    private LocationManager locationManager;
    Animation btnAnim ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash_temp);
        txt = findViewById(R.id.textView);
        getLocation();

        SharedPreferences preferences = getSharedPreferences("checkbox", 0);
        txt.setText(preferences.getString("username", "Log In"));

        txt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                SharedPreferences preferences = getSharedPreferences("checkbox", 0);
                final String checkbox = preferences.getString("remember", "false");
                final int userid = preferences.getInt("userID", 0);


                if (checkbox.equals("true") && userid != 0) {
                    Intent i = new Intent(Splash_temp.this, JobDetails.class);
                    startActivity(i);

                } else if (checkbox.equals("false") || userid == 0) {
                    Intent i = new Intent(Splash_temp.this, LoginActivity.class);
                    startActivity(i);

                }


            }
        });
        btnAnim = AnimationUtils.loadAnimation(getApplicationContext(),R.anim.button_1);
        txt.setAnimation(btnAnim);


    }

    public void getLocation() {
        try {
            locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 5000, 5, this);


        } catch (SecurityException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        Geocoder geocoder = new Geocoder(this, Locale.getDefault());
        latitude = location.getLatitude();
        longitude = location.getLongitude();
        Toast.makeText(Splash_temp.this, "Location Found", Toast.LENGTH_SHORT).show();
        SharedPreferences preferences = getSharedPreferences("checkbox",0);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("Latitude", String.valueOf(latitude));
        editor.putString("Longitude", String.valueOf(longitude));
        editor.apply();


    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {
        Toast.makeText(Splash_temp.this, "Please Enable GPS and Internet", Toast.LENGTH_SHORT).show();
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(Splash_temp.this);
        // Setting Dialog Title
        alertDialog.setTitle("GPS is settings");
        // Setting Dialog Message
        alertDialog.setMessage("GPS is not enabled. Do you want to go to settings menu?");
        // On pressing Settings button
        alertDialog.setPositiveButton("Settings", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivity(intent);
            }
        });
        alertDialog.setNegativeButton("Cancel",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        Dialog dialog = alertDialog.create();
        dialog.show();
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        // Showing Alert Message
        //alertDialog.show();
        //alertDialog.setCancelable(false);
    }
}
