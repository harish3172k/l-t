package com.example.ctc.Response;

public class UserLoginResponse {
    int userid;
    int roleid;

    public UserLoginResponse(int userid, int roleid) {
        this.userid = userid;
        this.roleid = roleid;
    }

    public UserLoginResponse() {
        super();
    }

    public int getUserid() {
        return userid;
    }

    public void setUserid(int userid) {
        this.userid = userid;
    }

    public int getRoleid() {
        return roleid;
    }

    public void setRoleid(int roleid) {
        this.roleid = roleid;
    }

    @Override
    public String toString() {
        return "UserLoginResponse{" +
                "userID=" + userid + ","
                + "roleID=" + roleid +
                '}';
    }
}
