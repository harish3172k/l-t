package com.example.ctc.Adapter;


import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import android.widget.TextView;

import com.example.ctc.R;


public class CiriaAdapter1 extends AppCompatActivity {
    TextView ac, bc, cc, dc, ec, fc, gc, hc, ic, jc, kc, lc, mc,nc;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result_ciria1);

        Intent intent1 =getIntent();
        double ELEMENT = intent1.getDoubleExtra("elementt",0);

        double C2 = intent1.getDoubleExtra("c2",0);
        double DENSITY = intent1.getDoubleExtra("density",0);
        double FORMHEIGHT = intent1.getDoubleExtra("verticalFormHeight",0);
        double POURHEIGHT = intent1.getDoubleExtra("verticalPourHeight",0);
        double VOLUME = intent1.getDoubleExtra("volume",0);
        double PLANAREA = intent1.getDoubleExtra("planArea",0);
        double TEMPERATURE = intent1.getDoubleExtra("temperature",0);
        double PMAX = intent1.getDoubleExtra("pmax",0);
        int JOBCODE  = intent1.getIntExtra("jobcode",0);
        String USERNAME = intent1.getStringExtra("userName");
        String JOBNAME = intent1.getStringExtra("jobName");
        String JOBLOCATION = intent1.getStringExtra("jobLocation");
        String CDATE=intent1.getStringExtra("Cdate_time");


        ac=findViewById(R.id.resultc1a);
        bc=findViewById(R.id.resultc2a);
        cc=findViewById(R.id.resultdena);
        dc=findViewById(R.id.resultfha);
        ec=findViewById(R.id.resultpha);
        fc=findViewById(R.id.resultvola);
        gc=findViewById(R.id.resultpaa);
        hc=findViewById(R.id.resulttempa);
        ic=findViewById(R.id.resultpmxa);
        jc=findViewById(R.id.resultcjobca);
        kc=findViewById(R.id.resultcuna);
        lc=findViewById(R.id.resultcjobna);
        mc=findViewById(R.id.resultcjobla);
        nc=findViewById(R.id.datec);




        cc.setText("TEMPERATURE:"+TEMPERATURE);
        ac.setText("PMAX:"+PMAX);
        bc.setText("VOLUME:"+VOLUME);
///////
        dc.setText("C1:"+ELEMENT);
        ec.setText("C2:"+C2);
        hc.setText("DENSITY:"+DENSITY);
        gc.setText("VERTICALFORMHEIGHT:"+FORMHEIGHT);
        fc.setText("VERTICALPOURHEIGHT:"+POURHEIGHT);
        ic.setText("RATE OF PLACEMENT:"+PLANAREA);
        jc.setText("JOBCODE:"+JOBCODE);
        kc.setText("USERNAME:"+USERNAME);
        lc.setText("JOBNAME:"+JOBNAME);
        mc.setText("JOBLOCATION:"+JOBLOCATION);
        nc.setText("DATE&TIME:"+CDATE);





    }
}
