package com.example.ctc.Activity;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.Settings;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.ctc.Beans.DashboardActivity;
import com.example.ctc.Beans.UserData;
import com.example.ctc.JobDetails;
import com.example.ctc.R;
import com.google.android.material.textfield.TextInputEditText;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.spark.submitbutton.SubmitButton;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.Locale;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.entity.StringEntity;

public class LoginActivity extends AppCompatActivity  {

    private TextInputEditText username, password;
    private SubmitButton loginBtn;
    private Spinner roleSpinner;
    private int roleID;
    private double latitude, longitude;

    CheckBox check;
    String checkbox;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);


        username = findViewById(R.id.un);
        password = findViewById(R.id.pass);
        loginBtn = findViewById(R.id.login);
        roleSpinner = findViewById(R.id.spinner);
        check=findViewById(R.id.checkBox);


        ArrayAdapter<String> roleAdapter = new ArrayAdapter<String>(LoginActivity.this, R.layout.support_simple_spinner_dropdown_item, getResources().getStringArray(R.array.roles));
        roleAdapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
        roleSpinner.setAdapter(roleAdapter);

        roleSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                String text = roleSpinner.getSelectedItem().toString();
                switch (text) {
                    case "Admin":
                        roleID = 1;
                        break;
                    case "Manager":
                        roleID = 2;
                        break;
                    case "Employee":
                        roleID = 3;
                        break;
                    case "User":
                        roleID = 4;
                        break;
                    default:
                        roleID = 0;
                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                Toast.makeText(LoginActivity.this, "Please Select Role", Toast.LENGTH_SHORT).show();
            }
        });


        check.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(buttonView.isChecked())
                {
                    SharedPreferences preferences = getSharedPreferences("checkbox",0);
                    SharedPreferences.Editor editor=preferences.edit();
                    editor.putString("remember","true");
                    editor.apply();
                    Toast.makeText(LoginActivity.this,"checked",Toast.LENGTH_SHORT).show();
                }
                else
                {
                    if(!buttonView.isChecked())
                    {
                        SharedPreferences preferences=getSharedPreferences("checkbox",MODE_PRIVATE);
                        SharedPreferences.Editor editor=preferences.edit();
                        editor.putString("remember","false");
                        editor.apply();;
                        Toast.makeText(LoginActivity.this,"Unchecked",Toast.LENGTH_SHORT).show();

                    }
                }
            }
        });

        loginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (username.getText().length() == 0) {
                    Toast.makeText(getApplicationContext(), "Please Enter User Name", Toast.LENGTH_SHORT).show();
                } else if (password.getText().length() == 0) {
                    Toast.makeText(getApplicationContext(), "Please Enter Password", Toast.LENGTH_SHORT).show();
                } else if (roleID == 0) {
                    Toast.makeText(getApplicationContext(), "Please Choose Role", Toast.LENGTH_SHORT).show();
                } else {
                    final String UserName = username.getText().toString();
                    final String Password = password.getText().toString();
                    final int Role = roleID;

                    SharedPreferences preferences=getSharedPreferences("checkbox",0);
                    SharedPreferences.Editor editor=preferences.edit();
                    editor.putString("username",username.getText().toString());
                    editor.apply();

                    AsyncHttpClient client = new AsyncHttpClient();
                    JSONObject jsonObject = new JSONObject();
                    StringEntity entity = null;
                    UserData userData = new UserData(UserName, Password, Role);
                    SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                   // SharedPreferences.Editor editor = settings.edit();
                    editor.putString("UserName", userData.getUserName());
                    editor.commit();
                    try {
                        entity = new StringEntity("{}");
                        jsonObject.put("UserName", userData.getUserName());
                        jsonObject.put("Password", userData.getPassword());
                        jsonObject.put("Role", userData.getRole());
                        entity = new StringEntity(jsonObject.toString());
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    client.post(LoginActivity.this, "http://rmkeclandt-env.miurmysbmy.us-east-2.elasticbeanstalk.com/userlogin", entity, "application/json", new AsyncHttpResponseHandler() {
                        @Override
                        public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                            try {
                                JSONObject jsonObject = new JSONObject(new String(responseBody, "UTF-8"));
                                SharedPreferences preferences = getSharedPreferences("checkbox",0);
                                SharedPreferences.Editor editor = preferences.edit();
                                editor.putInt("userID", jsonObject.getInt("userID"));
                                editor.apply();
                                validate(jsonObject.getInt("userID"), jsonObject.getInt("roleID"));
                            } catch (JSONException e) {
                                e.printStackTrace();
                            } catch (UnsupportedEncodingException e) {
                                e.printStackTrace();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                            try {
                                JSONObject jsonObject = new JSONObject(new String(responseBody, "UTF-8"));
                                Toast.makeText(getApplicationContext(), jsonObject.getString("message"), Toast.LENGTH_LONG).show();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            } catch (UnsupportedEncodingException e) {
                                e.printStackTrace();
                            }
                        }
                    });
                }
            }
        });
    }

    public void validate(int _userID, int _roleID) {
        if ((_userID != 0 && (_roleID == 1) || (_roleID == 2) || (_roleID == 3) || (_roleID == 4))) {
            Intent intent = new Intent(LoginActivity.this, JobDetails.class);
            startActivity(intent);
        }
    }


    }


















