package com.example.ctc.Beans;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.ctc.Activity.CiriaActivity;

import com.example.ctc.List.ListCiria;

import com.example.ctc.List.ListAci;

import com.example.ctc.List.ListJobs;
import com.example.ctc.R;
import com.example.ctc.Activity.ACIActivity;
import com.example.ctc.Response.CiriaResponse;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.entity.StringEntity;


public class DashboardActivity extends AppCompatActivity {

    CardView Aci,Ciria,Reportaci,Reportciria,Logout,reports;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);
        final SharedPreferences getPrefs = PreferenceManager.getDefaultSharedPreferences(getBaseContext());

       Aci=findViewById(R.id.aci);
       Ciria=findViewById(R.id.ciria);
       Reportciria=findViewById(R.id.ciriareport);
       Reportaci=findViewById(R.id.acireports);
       Logout=findViewById(R.id.logout);
       reports=findViewById(R.id.jobReports);

        AsyncHttpClient client = new AsyncHttpClient();
        JSONObject jsonObject = new JSONObject();
        StringEntity entity = null;
        final SharedPreferences preferences=getSharedPreferences("checkbox",0);
       try {
           entity = new StringEntity("{}");
           jsonObject.put("latitude",Double.valueOf(preferences.getString("Latitude","0.0")));
           jsonObject.put("longitude",Double.valueOf(preferences.getString("Longitude","0.0")));



           entity = new StringEntity(jsonObject.toString());
       } catch (UnsupportedEncodingException e) {
           e.printStackTrace();
       } catch (JSONException e) {
           e.printStackTrace();
       }
        client.post(DashboardActivity.this, "http://rmkeclandt-env.miurmysbmy.us-east-2.elasticbeanstalk.com/ciria", entity, "application/json", new AsyncHttpResponseHandler()
        {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                try {
                    Log.d("Demo Success: ", new JSONObject(new String(responseBody, "UTF-8")).toString());
                    JSONObject jsonObject = new JSONObject(new String(responseBody, "UTF-8"));
                    SharedPreferences.Editor editor=preferences.edit();
                    editor.putString("Temperature",String.valueOf(jsonObject.getDouble("temperature")));
                    editor.apply();
                } catch (JSONException e1) {
                    e1.printStackTrace();
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                try {
                    JSONObject jsonObject = new JSONObject(new String(responseBody, "UTF-8"));
                    Toast.makeText(DashboardActivity.this,"",Toast.LENGTH_LONG).show();

                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            }
        });


                Aci.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
               Toast.makeText(DashboardActivity.this,"You selected ACI",Toast.LENGTH_LONG).show();
               Intent intent=new Intent(DashboardActivity.this, ACIActivity.class);
               startActivity(intent);
           }
       });
       Ciria.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
               Toast.makeText(DashboardActivity.this,"You selected CIRIA",Toast.LENGTH_LONG).show();
               Intent intent=new Intent(DashboardActivity.this, CiriaActivity.class);
               startActivity(intent);
           }
       });
        Reportciria.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(DashboardActivity.this,"You selected CIRIA Reports",Toast.LENGTH_LONG).show();
                Intent intent=new Intent(DashboardActivity.this, ListCiria.class);
                startActivity(intent);
            }
        });
        Reportaci.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(DashboardActivity.this,"You selected ACI Reports",Toast.LENGTH_LONG).show();
                Intent intent=new Intent(DashboardActivity.this, ListAci.class);
                startActivity(intent);
            }
        });
        Logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(DashboardActivity.this,"Session Over",Toast.LENGTH_LONG).show();
                SharedPreferences preferences = getSharedPreferences("checkbox",0);
                SharedPreferences.Editor editor=preferences.edit();
                editor.putString("remember","false");
                editor.putInt("userID", 0);
                editor.putString("username",null);
                editor.putInt("Jobid",0);
                editor.putString("JobName","");
                editor.apply();
                finish();
                Intent i = getBaseContext().getPackageManager()
                        .getLaunchIntentForPackage( getBaseContext().getPackageName() );
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);


            }
        });
        reports.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(DashboardActivity.this,"You selected Job Reports",Toast.LENGTH_SHORT).show();
                Intent intent=new Intent(DashboardActivity.this, ListJobs.class);
                startActivity(intent);
            }
        });




    }
    @Override
    public void onBackPressed() { }


}
