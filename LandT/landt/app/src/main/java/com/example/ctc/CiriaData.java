package com.example.ctc;

public class CiriaData {
    double c2;
    double elementt;
    double density;
    double verticalFormHeight;
    double verticalPourHeight;
    double volume;
    double rop;
   // double temperature;


    //double rop1;



    public CiriaData(double c2, double elementt, double density, double verticalFormHeight, double verticalPourHeight, double volume, double rop) {
        super();
        this.c2 = c2;
        this.elementt=elementt;
        this.density = density;
        this.verticalFormHeight = verticalFormHeight;
        this.verticalPourHeight = verticalPourHeight;
        this.volume = volume;
        this.rop = rop;
        //this.temperature = temperature;
        //this.rop1=rop1;
    }


    public double getC2() {
        return c2;
    }

    public void setC2(double c2) {
        this.c2 = c2;
    }
    public double getElementt(){
        return elementt;
    }
    public void setElementt(double elementt){
        this.elementt=elementt;
    }

    public double getDensity() {
        return density;
    }

    public void setDensity(double density) {
        this.density = density;
    }

    public double getVerticalFormHeight() {
        return verticalFormHeight;
    }

    public void setVerticalFormHeight(double verticalFormHeight) {
        this.verticalFormHeight = verticalFormHeight;
    }

    public double getVerticalPourHeight() {
        return verticalPourHeight;
    }

    public void setVerticalPourHeight(double verticalPourHeight) {
        this.verticalPourHeight = verticalPourHeight;
    }

    public double getVolume() {
        return volume;
    }

    public void setVolume(double volume) {
        this.volume = volume;
    }

    public double getRop() {
        return rop;
    }

    public void setRop(double rop) {
        this.rop = rop;
    }



   /* public double getRop1()
    {
        return rop1;
    }
    public void setRop1(double rop1)
    {
        this.rop1=rop1;
    }*/

}
