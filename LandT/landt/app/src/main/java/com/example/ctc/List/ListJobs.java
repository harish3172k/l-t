package com.example.ctc.List;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.example.ctc.Adapter.JobsAdapter;
import com.example.ctc.Item.JobItem;
import com.example.ctc.R;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.entity.StringEntity;

public class ListJobs extends AppCompatActivity  {

    RecyclerView jobsRecylerView;
    JobsAdapter jobsAdapter;
    List<JobItem> jobResponse;
    RequestQueue mRequestQueue;
    AsyncHttpClient client = new AsyncHttpClient();
    JSONObject jsonObject = new JSONObject();
    StringEntity entity = null;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_jobs);
        final SharedPreferences preferences = getSharedPreferences("checkbox",0);

        jobsRecylerView = findViewById(R.id.jobsRecylerView);
        jobResponse = new ArrayList<>();
        mRequestQueue= Volley.newRequestQueue(this);
        try {
            entity = new StringEntity("{}");
            jsonObject.put("userID", preferences.getInt("userID", 0));
            entity = new StringEntity(jsonObject.toString());
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }



        client.post(ListJobs.this, "http://rmkeclandt-env.miurmysbmy.us-east-2.elasticbeanstalk.com/getjobdetailsofuser", entity, "application/json", new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                try {
                    JSONObject jsonObject = new JSONObject(new String(responseBody));
                    int jobcount =  jsonObject.getInt("count");
                    JSONArray jobArray =  jsonObject.getJSONArray("data");
                    List<JobItem> jobResponse = new ArrayList<>();

                    for (int i = 0; i<jobcount;i++) {
                        JobItem items = new JobItem();
                        JSONObject obj = jobArray.getJSONObject(i);
                        items.setJobID(obj.getInt("jobID"));
                        items.setJobName(obj.getString("jobName"));
                        items.setJobCode(obj.getString("jobcode"));
                        items.setJobLocation(obj.getString("jobLocation"));
                        items.setCluster(obj.getString("cluster"));
                        items.setStructureName(obj.getString("nameOfStructure"));
                        items.setElementName(obj.getString("nameofElement"));
                        items.setDuration(obj.getInt("duration"));
                        items.setDatejob(obj.getString("createdAt"));
                        items.setUsername(preferences.getString("UserName",""));
                        obj.getInt("userID");
                        jobResponse.add(items);


                       /* editor.putString("job_code",String.valueOf(items.getJobID()));
                        editor.putString("job_name",items.getJobName());
                        editor.putString("job_location",items.getJobCode());
                        editor.putString("cluster",items.getCluster());
                        editor.putString("user_name",items.getJobLocation());
                        editor.putString("date",String.valueOf(items.getJobID()));
                        editor.putString("structure",items.getStructureName());
                        editor.putString("elementj",items.getElementName());
                        editor.putString("durationj",String.valueOf(items.getDuration()));
                        editor.commit();
                        editor.clear();
                        editor.commit();*/


                    }
                    jobsAdapter = new JobsAdapter(ListJobs.this, jobResponse);
                    jobsRecylerView.setAdapter(jobsAdapter);
                    jobsRecylerView.setLayoutManager(new LinearLayoutManager(ListJobs.this));






                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                try {
                    JSONObject jsonObject = new JSONObject(new String(responseBody, "UTF-8"));
                    Toast.makeText(getApplicationContext(), jsonObject.getString("details"), Toast.LENGTH_LONG).show();
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }

            }

        });





    }

   /* @Override
    public void onItemClick(int position) {
        Intent detailIntent=new Intent(this,JobAdapter1.class);
        JobItem clickedItem=jobResponse.get(position);

        detailIntent.putExtra("jobID",clickedItem.getJobID());
        detailIntent.putExtra("duration",clickedItem.getDuration());
        detailIntent.putExtra("jobCode",clickedItem.getJobCode());
        detailIntent.putExtra("jobName",clickedItem.getJobName());
        detailIntent.putExtra("jobLocation",clickedItem.getJobLocation());
        detailIntent.putExtra("cluster",clickedItem.getCluster());
        detailIntent.putExtra("structureName",clickedItem.getStructureName());
        detailIntent.putExtra("elementName",clickedItem.getElementName());
        startActivity(detailIntent);

    }*/
}
