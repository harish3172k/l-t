package com.example.ctc.Item;

public class CiriaItems {


    int ciriaID1, jobID1, duration1, role1, userID1, userID2;
    String latitude1, cluster1, jobCode1, longtitude1, formulaversion1, jobName1, jobLocation1, nameOfStructure1, nameofElement1, name1, branch1, userName1;
    double elementt1;
    double c21;
    double density1;
    double verticalFormHeight1;
    double verticalPourHeight1;
    double volume1;
    double planArea1;
    double temperature1;
    double pmax1;
    String createdON1;




    public CiriaItems(int ciriaID1,
                      int jobID1,
                      int userID1,
                      String latitude1,
                      String longtitude1,
                      String formulaversion1,
                      double elementt1,
                      double c21,
                      double density1,
                      double verticalFormHeight1,
                      double verticalPourHeight1,
                      double volume1,
                      double planArea1,
                      double temperature1,
                      double pmax1,
                      String createdON1,
                      String jobCode1,
                      String cluster1,
                      String jobName1,
                      String jobLocation1,
                      String nameOfStructure1,
                      String nameofElement1,
                      int duration1,
                      String name1,
                      String branch1,
                      String userName1,
                      int role1,
                      int userID2)
    {

        this.ciriaID1 = ciriaID1;
        this.jobID1 = jobID1;
        this.duration1 = duration1;
        this.role1 = role1;
        this.userID1 = userID1;
        this.userID2 = userID2;
        this.latitude1 = latitude1;
        this.cluster1 = cluster1;
        this.jobCode1 = jobCode1;
        this.longtitude1 = longtitude1;
        this.formulaversion1 = formulaversion1;
        this.jobName1 = jobName1;
        this.jobLocation1 = jobLocation1;
        this.nameOfStructure1 = nameOfStructure1;
        this.nameofElement1 = nameofElement1;
        this.name1 = name1;
        this.branch1 = branch1;
        this.userName1 = userName1;
        this.elementt1 = elementt1;
        this.c21 = c21;
        this.density1 = density1;
        this.verticalFormHeight1 = verticalFormHeight1;
        this.verticalPourHeight1 = verticalPourHeight1;
        this.volume1 = volume1;
        this.planArea1 = planArea1;
        this.temperature1 = temperature1;
        this.pmax1 = pmax1;
        this.createdON1 = createdON1;


    }

    public CiriaItems() {

    }


    public int getCiriaID1() {
        return ciriaID1;
    }

    public void setCiriaID1(int ciriaID1) {
        this.ciriaID1 = ciriaID1;
    }


    public int getRole1() {
        return role1;
    }

    public void setRole1(int role1) {
        this.role1 = role1;
    }

    public int getUserID1() {
        return userID1;
    }

    public void setUserID1(int userID1) {
        this.userID1 = userID1;
    }

    public int getUserID2() {
        return userID2;
    }

    public void setUserID2(int userID2) {
        this.userID2 = userID2;
    }

    public String getLatitude1() {
        return latitude1;
    }

    public void setLatitude1(String latitude1) {
        this.latitude1 = latitude1;
    }

    public String getCluster1() {
        return cluster1;
    }

    public void setCluster1(String cluster1) {
        this.cluster1 = cluster1;
    }

    public String getJobCode1() {
        return jobCode1;
    }

    public void setJobCode1(String jobCode1) {
        this.jobCode1 = jobCode1;
    }

    public String getLongtitude1() {
        return longtitude1;
    }

    public void setLongtitude1(String longtitude1) {
        this.longtitude1 = longtitude1;
    }

    public String getFormulaversion1() {
        return formulaversion1;
    }

    public void setFormulaversion1(String formulaversion1) {
        this.formulaversion1 = formulaversion1;
    }

    public String getJobName1() {
        return jobName1;
    }

    public void setJobName1(String jobName1) {
        this.jobName1 = jobName1;
    }

    public String getJobLocation1() {
        return jobLocation1;
    }

    public void setJobLocation1(String jobLocation1) {
        this.jobLocation1 = jobLocation1;
    }

    public String getNameOfStructure1() {
        return nameOfStructure1;
    }

    public void setNameOfStructure1(String nameOfStructure1) {
        this.nameOfStructure1 = nameOfStructure1;
    }

    public String getNameofElement1() {
        return nameofElement1;
    }

    public void setNameofElement1(String nameofElement1) {
        this.nameofElement1 = nameofElement1;
    }

    public String getName1() {
        return name1;
    }

    public void setName1(String name1) {
        this.name1 = name1;
    }

    public String getBranch1() {
        return branch1;
    }

    public void setBranch1(String branch1) {
        this.branch1 = branch1;
    }

    public String getUserName1() {
        return userName1;
    }

    public void setUserName1(String userName1) {
        this.userName1 = userName1;
    }

    public double getElementt1() {
        return elementt1;
    }

    public void setElementt1(double elementt1) {
        this.elementt1 = elementt1;
    }

    public double getC21() {
        return c21;
    }

    public void setC21(double c21) {
        this.c21 = c21;
    }

    public double getDensity1() {
        return density1;
    }

    public void setDensity1(double density1) {
        this.density1 = density1;
    }

    public double getVerticalFormHeight1() {
        return verticalFormHeight1;
    }

    public void setVerticalFormHeight1(double verticalFormHeight1) {
        this.verticalFormHeight1 = verticalFormHeight1;
    }

    public double getVerticalPourHeight1() {
        return verticalPourHeight1;
    }

    public void setVerticalPourHeight1(double verticalPourHeight1) {
        this.verticalPourHeight1 = verticalPourHeight1;
    }

    public double getVolume1() {
        return volume1;
    }

    public void setVolume1(double volume1) {
        this.volume1 = volume1;
    }

    public double getPlanArea1() {
        return planArea1;
    }

    public void setPlanArea1(double planArea1) {
        this.planArea1 = planArea1;
    }

    public double getTemperature1() {
        return temperature1;
    }

    public void setTemperature1(double temperature1) {
        this.temperature1 = temperature1;
    }

    public double getPmax1() {
        return pmax1;
    }

    public void setPmax1(double pmax1) {
        this.pmax1 = pmax1;
    }

    public String getCreatedON1() {
        return createdON1;
    }

    public void setCreatedON1(String createdON1) {
        this.createdON1 = createdON1;
    }

    public int getJobID1() {
        return jobID1;
    }

    public void setJobID1(int jobID1) {
        this.jobID1 = jobID1;
    }
}


