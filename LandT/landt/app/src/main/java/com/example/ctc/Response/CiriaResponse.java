package com.example.ctc.Response;

public class CiriaResponse {
    double pmax;

    public CiriaResponse() {
        super();
    }

    public CiriaResponse(double pmax) {
        super();
        this.pmax = pmax;
    }

    public double getPmax() {
        return pmax;
    }

    public void setPmax(double pmax) {
        this.pmax = pmax;
    }

    @Override
    public String toString() {
        return "CiriaResponse{" +
                "pmax=" + pmax +
                '}';
    }
}
