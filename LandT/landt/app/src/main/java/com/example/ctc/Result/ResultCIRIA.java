package com.example.ctc.Result;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;

import com.example.ctc.R;

public class ResultCIRIA extends AppCompatActivity {

    TextView Ac, Bc, Cc;
    TextView Dc, Ec, Fc;
    TextView Gc, Hc, Ic, Jc, Kc, Lc, Mc;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.current_ciria);
        final SharedPreferences preferences = getSharedPreferences("checkbox",0);

        Ac = findViewById(R.id.resultc1);
        Bc = findViewById(R.id.resultc2);
        Cc = findViewById(R.id.resultden);
        Dc = findViewById(R.id.resultfh);
        Ec = findViewById(R.id.resultph);
        Fc = findViewById(R.id.resultvol);
        Gc = findViewById(R.id.resultpa);
        Hc = findViewById(R.id.resulttemp);
        Ic = findViewById(R.id.resultpmx);
        Jc = findViewById(R.id.resultcjobc);
        Kc = findViewById(R.id.resultcun);
        Lc = findViewById(R.id.resultcjobn);
        Mc = findViewById(R.id.resultcjobl);


        Ac.setText("Pmax :" + preferences.getString("Pmax", null));
        Cc.setText("Temperature :" + preferences.getString("Temperature1", null));
        Bc.setText("Volume :" + preferences.getString("Volume", null));
//////
        Dc.setText("C1 :" + preferences.getString("c1", null));
        Ec.setText("C2 :" + preferences.getString("c2", null));
        Hc.setText("Density :" + preferences.getString("Density", null));
        Gc.setText("Form Height :" + preferences.getString("Form_Height", null));
        Fc.setText("Pour Height :" + preferences.getString("Pour_Height", null));
        Ic.setText("Rate :" + preferences.getString("Rate", null));
        Jc.setText("JobCode :" + preferences.getString("JobCode", null));
        Kc.setText("UserName :" + preferences.getString("UserName", null));
        Lc.setText("JobName :" + preferences.getString("JobName", null));
        Mc.setText("JobLocation :" + preferences.getString("JobLocation", null));


    }
}

