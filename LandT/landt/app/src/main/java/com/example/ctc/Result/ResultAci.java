package com.example.ctc.Result;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;

import androidx.appcompat.app.AppCompatActivity;

import android.widget.TextView;

import com.example.ctc.R;

public class ResultAci extends AppCompatActivity {


    TextView textivd, textslump, textdensity, textvfh, textvolume, textplanarea, texttemp, textpmax, textjobcode, textusername, textjobname, textjoblocation, textslag, textflyash, textretarders, textwallheight, textelement;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.current_aci);
        final SharedPreferences preferences = getSharedPreferences("checkbox",0);


        textivd = findViewById(R.id.resultaivd);
        textslump = findViewById(R.id.resultaslump);
        textdensity = findViewById(R.id.resultaden);
        textvfh = findViewById(R.id.resultafh);
        textvolume = findViewById(R.id.resultavol);
        textplanarea = findViewById(R.id.resultapa);
        texttemp = findViewById(R.id.resultatemp);
        textpmax = findViewById(R.id.resultapmx);
        textjobcode = findViewById(R.id.resultajobc);
        textusername = findViewById(R.id.resultacun);
        textjobname = findViewById(R.id.resultajobn);
        textjoblocation = findViewById(R.id.resultajobl);
        textslag = findViewById(R.id.resultajobslag);
        textflyash = findViewById(R.id.resultajobflyash);
        textretarders = findViewById(R.id.resultajobretarders);
        textwallheight = findViewById(R.id.resultajobwallheight);
        textelement = findViewById(R.id.resultaele);


        textelement.setText("Temperature :" + preferences.getString("Temperature2", null));
        textivd.setText("CCPMAX :" + preferences.getString("ccpmax", null));
        textslump.setText("Volume :" + preferences.getString("volume", null));

////////
        textpmax.setText("Internal Vibration Depth :" + preferences.getString("ivd", null));
        textvolume.setText("Slump :" + preferences.getString("slump", null));
        texttemp.setText("Density :" + preferences.getString("density", null));
        textvfh.setText("Vertical Form Height :" + preferences.getString("vfh", null));
        textplanarea.setText("Rate :" + preferences.getString("ratee", null));
        textjobcode.setText("JobCode :" + preferences.getString("JobCode", null));
        textusername.setText("UserName :" + preferences.getString("UserName", ""));
        textjobname.setText("JobName :" + preferences.getString("JobName", null));
        textjoblocation.setText("JobLocation :" + preferences.getString("JobLocation", ""));
        textslag.setText("Slag :" + preferences.getString("slag", null));
        textflyash.setText("Flyash :" + preferences.getString("flyash", null));
        textretarders.setText("Retarders :" + preferences.getString("retarders", null));
        textwallheight.setText("Wall Height :" + preferences.getString("wallheight", null));
        //////
        textdensity.setText("Element :" + preferences.getString("element", null));


    }
}
