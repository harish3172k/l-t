<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!-- Meta, title, CSS, favicons, etc. -->
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<title>RMKEC | L & T | Form Work Pressure Caclulator Login</title>

<!-- Bootstrap -->
<link href="../vendors/bootstrap/dist/css/bootstrap.min.css"
	rel="stylesheet">
<!-- Font Awesome -->
<link href="../vendors/font-awesome/css/font-awesome.min.css"
	rel="stylesheet">
<!-- NProgress -->
<link href="../vendors/nprogress/nprogress.css" rel="stylesheet">
<!-- Animate.css -->
<link href="../vendors/animate.css/animate.min.css" rel="stylesheet">

<!-- Custom Theme Style -->
<link href="../build/css/custom.min.css" rel="stylesheet">
</head>

<body class="login">
	<div>
		<a class="hiddenanchor" id="signup"></a> <a class="hiddenanchor"
			id="signin"></a>

		<div class="login_wrapper">
			<div class="animate form login_form">
				<section class="login_content">
					<form method="post" id="loginForm">
						<h1>Form Pressure Calculator Login Form</h1>
						<div>
							<input type="text" class="form-control" placeholder="Username"
								required="" />
						</div>
						<div>
							<input type="password" class="form-control"
								placeholder="Password" required="" />
						</div>
						<div>
							<input type="submit" class="btn btn-default submit" value="Submit">
						</div>
						<div class="clearfix"></div>
					</form>
				</section>
			</div>
		</div>
	</div>
	 <!-- jQuery -->
    <script src="../vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="../vendors/bootstrap/dist/js/bootstrap.min.js"></script>
	<script type="text/javascript">
		$(document).ready(function() {
			var $form = $("#loginForm");
			$form.on('submit', function(e) {
				alert();
				e.preventDefault(); // stop default form submission 
				$.ajax({ // form submission via ajax
					url : $form.attr('action'), // form submission url
					type : 'POST', // request type
					dataType : 'json', // data type
					data : $form.serialize(), // get all data from the form
					success : function(result) {
						console.log(result); // response back from server in case of success
					},
					error : function(xhr, resp, text) { // response back from server in case of failure 
						console.log(xhr, resp, text);
					}
				})
			});
		});
	</script>
</body>
</html>
