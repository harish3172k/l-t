package com.landt.rmkec.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.landt.rmkec.beans.ACIBean;
import com.landt.rmkec.exception.ACIException;
import com.landt.rmkec.util.DBUtil;

public class ACIDAO {
	public String Status = "FAILURE";

	Connection connection;
	PreparedStatement stmt;
	ResultSet result;
	int id;

	public int createRecordACI(ACIBean aciData) throws ACIException {
		try {
			connection = DBUtil.getDBConnection();
			String sql = "INSERT INTO aci(jobid,userid,elementtype,retarders,cementtype,internalvibration,slag,slump,flyash,wallheight,den,volume,verticalformheight,ratee,latitude,longtiude,temperature,pmax) "
					+ "		VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
			stmt = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
			stmt.setInt(1, aciData.getJobID());
			stmt.setInt(2, aciData.getUserId());
			stmt.setString(3, aciData.getElementType());
			stmt.setString(4, aciData.getRetarders());
			stmt.setString(5, aciData.getCementType());
			stmt.setString(6, aciData.getInternalVibration());
			stmt.setString(7, aciData.getSlag());
			stmt.setString(8, aciData.getSlump());
			stmt.setString(9, aciData.getFlyash());
			stmt.setString(10, aciData.getWallheight());
			stmt.setDouble(11, aciData.getDen());
			stmt.setDouble(12, aciData.getVolume());
			stmt.setDouble(13, aciData.getVerticalformheight());
			stmt.setDouble(14, aciData.getRatee());
			stmt.setString(15, aciData.getLatitude());
			stmt.setString(16, aciData.getLongtiude());
			stmt.setDouble(17, aciData.getTemperatureee());
			stmt.setDouble(18, aciData.getPmax());
			stmt.executeUpdate();
			result = stmt.getGeneratedKeys();
			if (result.next()) {
				id = result.getInt(1);
			} else {
				throw new ACIException("Data cannot be stored in Database");
			}
		} catch (Exception e) {
			throw new ACIException("Database Exception" + e);
		}
		return id;
	}

	public List<ACIBean> getallACIDetailsfromDB(ACIBean aci) throws ACIException {
		List<ACIBean> acidetails = new ArrayList<ACIBean>();
		try {
			String sql = "SELECT aci.id,aci.jobid,aci.userid,aci.elementtype,aci.retarders,aci.cementtype,aci.internalvibration,aci.slag,aci.slump,aci.flyash,aci.wallheight,aci.den,aci.volume,aci.verticalformheight,aci.ratee,aci.latitude,aci.longtiude,aci.temperature,aci.pmax,aci.createdat,jobdetails.id,jobdetails.jobcode,jobdetails.userid,jobdetails.cluster,jobdetails.jobname,jobdetails.joblocation,jobdetails.nameofstructure,jobdetails.nameofelement,jobdetails.duration,users.id,users.username,users.role FROM aci INNER JOIN jobdetails ON aci.jobid=jobdetails.id INNER JOIN users ON users.id=users.id where aci.userid=? GROUP BY aci.id";
			connection = DBUtil.getDBConnection();
			stmt = connection.prepareStatement(sql);
			stmt.setInt(1, aci.getUserId());
			result = stmt.executeQuery();
			while (result.next()) {
				acidetails.add(new ACIBean(result.getInt("aci.id"), result.getInt("aci.jobid"),
						result.getInt("aci.userid"), 
						result.getString("aci.elementtype"), result.getString("aci.cementtype"),
						result.getString("aci.internalvibration"), result.getString("aci.slag"),
						result.getString("aci.flyash"), result.getString("aci.retarders"),
						result.getString("aci.slump"), result.getString("aci.wallheight"),
						result.getString("aci.latitude"), result.getString("aci.longtiude"),
						result.getDouble("aci.den"), result.getDouble("aci.volume"),
						result.getDouble("aci.verticalformheight"), result.getDouble("aci.ratee"),
						result.getDouble("aci.temperature"), result.getDouble("aci.pmax"),
						result.getString("aci.createdat"), result.getString("jobdetails.jobcode"),
						result.getString("jobdetails.cluster"), result.getString("jobdetails.jobname"),
						result.getString("jobdetails.joblocation"), result.getString("jobdetails.nameofstructure"),
						result.getString("jobdetails.nameofelement"), result.getInt("jobdetails.duration"),
						result.getString("users.username"), result.getInt("users.role")));
			}
		} catch (Exception e) {
			throw new ACIException("Database Exception" + e);
		}
		return acidetails;
	}

	/*
	 * public List<ACIBean> getACIDetailfromDB(ACIBean aci) throws ACIException {
	 * List<ACIBean> acidetails = new ArrayList<ACIBean>(); try { String sql =
	 * "SELECT aci.id,aci.jobid,aci.userid,aci.formulaversion,aci.elementtype,aci.retarders,aci.typeofcement,aci.vibrationlength,aci.slag,aci.slump,aci.flyash,aci.wallheight,aci.density,aci.volume,aci.verticalformheight,aci.planarea,aci.latitude,aci.longtitude,aci.temperature,aci.pmax,aci.createdat,jobdetails.jobid,jobdetails.jobcode,jobdetails.userid,jobdetails.cluster,jobdetails.jobname,jobdetails.joblocation,jobdetails.nameofstructure,jobdetails.nameofelement,jobdetails.duration,users.userid,users.name,users.branch,users.username,users.role FROM aci INNER JOIN jobdetails ON aci.jobid=jobdetails.jobid INNER JOIN users ON users.userid=users.userid where aci.id=?"
	 * ; connection = DBUtil.getDBConnection(); stmt =
	 * connection.prepareStatement(sql); stmt.setInt(1, aci.getAciID()); result =
	 * stmt.executeQuery(); while (result.next()) { acidetails.add(new
	 * ACIBean(result.getInt("aci.id"), result.getInt("aci.jobid"),
	 * result.getInt("aci.userid"), result.getString("aci.formulaversion"),
	 * result.getString("aci.elementtype"), result.getString("aci.typeofcement"),
	 * result.getString("aci.vibrationlength"), result.getString("aci.slag"),
	 * result.getString("aci.flyash"), result.getString("aci.retarders"),
	 * result.getString("aci.slump"), result.getString("aci.wallheight"),
	 * result.getString("aci.latitude"), result.getString("aci.longtitude"),
	 * result.getDouble("aci.density"), result.getDouble("aci.volume"),
	 * result.getDouble("aci.verticalformheight"), result.getDouble("aci.ratee"),
	 * result.getDouble("aci.temperature"), result.getDouble("aci.pmax"),
	 * result.getString("aci.createdat"), result.getInt("jobdetails.jobcode"),
	 * result.getString("jobdetails.cluster"),
	 * result.getString("jobdetails.jobname"),
	 * result.getString("jobdetails.joblocation"),
	 * result.getString("jobdetails.nameofstructure"),
	 * result.getString("jobdetails.nameofelement"),
	 * result.getInt("jobdetails.duration"), result.getString("users.name"),
	 * result.getString("users.branch"), result.getString("users.username"),
	 * result.getInt("users.role"))); }
	 * 
	 * } catch (Exception e) { throw new ACIException("Database Exception" + e); }
	 * return acidetails; }
	 */

	public boolean ACIidinDB(int ACIID) throws ACIException {
		boolean status = true;
		try {
			String sql = "SELECT id FROM aci WHERE id=?";
			Connection connection = DBUtil.getDBConnection();
			PreparedStatement stmt = connection.prepareStatement(sql);
			stmt.setInt(1, ACIID);
			ResultSet result = stmt.executeQuery();
			if (result.next()) {
				status = false;
			}
		} catch (SQLException e) {
			throw new ACIException("Database Exception" + e);
		}
		return status;
	}

	public boolean ACIIDforUser(int ACIID, int userID) throws ACIException {
		boolean status = true;
		try {
			String sql = "SELECT id FROM aci WHERE id=? and userid=?";
			Connection connection = DBUtil.getDBConnection();
			PreparedStatement stmt = connection.prepareStatement(sql);
			stmt.setInt(1, ACIID);
			stmt.setInt(2, userID);
			ResultSet result = stmt.executeQuery();
			if (result.next()) {
				status = false;
			}
		} catch (Exception e) {
			throw new ACIException("Database Exception");
		}
		return status;
	}

}
