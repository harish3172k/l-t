package com.landt.rmkec.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import com.landt.rmkec.beans.UserLoginBean;
import com.landt.rmkec.exception.UserLoginException;
import com.landt.rmkec.util.DBUtil;

public class UserLoginDAO {

	int userid;
	Connection connection = DBUtil.getDBConnection();
	PreparedStatement stmt;
	ResultSet result;

	public int CheckUserLogin(UserLoginBean userLoginData) {
		try {
			Connection connection = DBUtil.getDBConnection();
			String sql = "select * from users where username = ? and password=? and role=?";
			stmt = connection.prepareStatement(sql);
			stmt.setString(1, userLoginData.getUserName());
			stmt.setString(2, userLoginData.getPassword());
			stmt.setInt(3, userLoginData.getRole());
			result = stmt.executeQuery();
			if (result.next())
				userid = result.getInt(1);
			else
				throw new UserLoginException("User Login Failed! Check User Name and Password");
		} catch (Exception e) {

			throw new UserLoginException("Error in Database" +e);

			

		}
		return userid;
	}
}
