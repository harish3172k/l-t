package com.landt.rmkec.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.landt.rmkec.beans.JobDetailsBean;
import com.landt.rmkec.beans.JobDetailsUserIDBean;
import com.landt.rmkec.exception.JobDetailsException;
import com.landt.rmkec.util.DBUtil;

@Transactional
@Repository
public class JobDetailsDAO {

	public int id = 0;
	Connection connection;
	PreparedStatement stmt;
	ResultSet result;

	public int StoreJobDetails(JobDetailsBean jobDetails) throws JobDetailsException {
		try {
			connection = DBUtil.getDBConnection();
			String sql = "INSERT INTO jobdetails (userid,cluster,jobcode, jobname,joblocation,nameofstructure,nameofelement,duration) VALUES(?,?,?,?,?,?,?,?)";
			stmt = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
			stmt.setInt(1, jobDetails.getUserID());
			stmt.setString(2, jobDetails.getCluster());
			stmt.setString(3, jobDetails.getJobcode());
			stmt.setString(4, jobDetails.getJobName());
			stmt.setString(5, jobDetails.getJobLocation());
			stmt.setString(6, jobDetails.getNameOfStructure());
			stmt.setString(7, jobDetails.getNameofElement());
			stmt.setInt(8, jobDetails.getDuration());
			stmt.executeUpdate();
			result = stmt.getGeneratedKeys();
			if (result.next()) {
				id = result.getInt(1);
			} else {
				throw new JobDetailsException("Data not be added to Database");
			}
		} catch (Exception e) {
			throw new JobDetailsException("Database Error");
		}
		return id;
	}

	public List<JobDetailsBean> getJobDetailsfromDB(JobDetailsUserIDBean jb) throws JobDetailsException {
		List<JobDetailsBean> jobdetails = new ArrayList<JobDetailsBean>();
		try {
			String sql = "SELECT id,jobcode,userid,cluster,jobname,joblocation,nameofstructure,nameofelement,duration,createdat FROM jobdetails WHERE userid=? GROUP BY jobdetails.id";
			connection = DBUtil.getDBConnection();
			stmt = connection.prepareStatement(sql);
			stmt.setInt(1, jb.getUserID());
			result = stmt.executeQuery();
			while (result.next()) {
				jobdetails.add(new JobDetailsBean(result.getInt("id"), result.getString("jobcode"),
						result.getInt("userid"), result.getString("cluster"), result.getString("jobname"),
						result.getString("joblocation"), result.getString("nameofstructure"),
						result.getString("nameofelement"), result.getInt("duration"), result.getString("createdat")));
			}
		} catch (Exception e) {
			throw new JobDetailsException("Data not be found in Database");
		}
		return jobdetails;
	}

	public boolean UserinDB(int userID) throws JobDetailsException {
		boolean status = true;
		try {
			String sql = "SELECT id FROM users WHERE id=?";
			Connection connection = DBUtil.getDBConnection();
			PreparedStatement stmt = connection.prepareStatement(sql);
			stmt.setInt(1, userID);
			ResultSet result = stmt.executeQuery();
			if (result.next()) {
				status = false;
			}
		} catch (Exception e) {
			throw new JobDetailsException("User ID not be found in Database");
		}
		return status;
	}

	public boolean JobinDB(int jobID) throws JobDetailsException {
		boolean status = true;
		try {
			String sql = "SELECT id FROM jobdetails WHERE id=?";
			Connection connection = DBUtil.getDBConnection();
			PreparedStatement stmt = connection.prepareStatement(sql);
			stmt.setInt(1, jobID);
			ResultSet result = stmt.executeQuery();
			if (result.next()) {
				status = false;
			}
		} catch (Exception e) {
			throw new JobDetailsException("Job ID not found in Database");
		}
		return status;
	}

}
