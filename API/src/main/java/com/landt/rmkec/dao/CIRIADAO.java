package com.landt.rmkec.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.landt.rmkec.beans.ACIBean;
import com.landt.rmkec.beans.CIRIABean;
import com.landt.rmkec.beans.JobDetailsBean;
import com.landt.rmkec.beans.UserLoginBean;
import com.landt.rmkec.exception.ACIException;
import com.landt.rmkec.exception.CIRIAException;
import com.landt.rmkec.util.DBUtil;

public class CIRIADAO {
	public String Status = "FAILURE";
	Connection connection;
	PreparedStatement stmt;
	ResultSet result;
	int id;

	public int createRecordCIRIA(CIRIABean ciriaData) throws CIRIAException {
		try {
			connection = DBUtil.getDBConnection();
			String sql = "INSERT INTO ciria(jobid,userid,typeofelement,cementtype, density,verticalformheight,verticalpourheight,rate,latitude,longtitude,temperature,pmax,volume) "
					+ "VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?)";
			stmt = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
			stmt.setInt(1, ciriaData.getJobID());
			stmt.setInt(2, ciriaData.getUserId());
			stmt.setDouble(3, ciriaData.getElementt());
			stmt.setDouble(4, ciriaData.getC2());
			stmt.setDouble(5, ciriaData.getDensity());
			stmt.setDouble(6, ciriaData.getVerticalFormHeight());
			stmt.setDouble(7, ciriaData.getVerticalPourHeight());
			stmt.setDouble(8, ciriaData.getRate());
			stmt.setString(9, ciriaData.getLatitude());
			stmt.setString(10, ciriaData.getLongtitude());
			stmt.setDouble(11, ciriaData.getTemperature());
			stmt.setDouble(12, ciriaData.getPmax());
			stmt.setDouble(13, ciriaData.getVolume());
			stmt.executeUpdate();
			result = stmt.getGeneratedKeys();
			if (result.next()) {
				id = result.getInt(1);
			} else {
				throw new CIRIAException("Data cannot be stored in Database");
			}
		} catch (Exception e) {
			throw new CIRIAException("Database Error" + e);
		}
		return id;
	}

	public List<CIRIABean> getallCIRIADetailsfromDB(CIRIABean ciria) throws CIRIAException {
		List<CIRIABean> ciriadetails = new ArrayList<CIRIABean>();
		try {
			String sql = "SELECT ciria.id,ciria.jobid,ciria.userid,ciria.typeofelement,ciria.cementtype,ciria.density,ciria.verticalformheight,ciria.verticalpourheight,ciria.volume,ciria.rate,ciria.latitude,ciria.longtitude,ciria.temperature,ciria.pmax,ciria.createdat,jobdetails.id,jobdetails.jobcode,jobdetails.userid,jobdetails.cluster,jobdetails.jobname,jobdetails.joblocation,jobdetails.nameofstructure,jobdetails.nameofelement,jobdetails.duration,users.id,users.username,users.role FROM ciria INNER JOIN jobdetails ON ciria.jobid=jobdetails.id INNER JOIN users ON users.id=users.id where ciria.userid=? GROUP BY ciria.id";
			connection = DBUtil.getDBConnection();
			stmt = connection.prepareStatement(sql);
			stmt.setInt(1, ciria.getUserId());
			result = stmt.executeQuery();
			while (result.next()) {
				ciriadetails.add(new CIRIABean(result.getInt("ciria.id"), result.getInt("ciria.jobid"),
						result.getInt("ciria.userid"), result.getString("ciria.latitude"),
						result.getString("ciria.longtitude"),
						result.getDouble("ciria.typeofelement"), result.getDouble("ciria.cementtype"),
						result.getDouble("ciria.density"), result.getDouble("ciria.verticalformheight"),
						result.getDouble("ciria.verticalpourheight"), result.getDouble("ciria.volume"),
						result.getDouble("ciria.rate"), result.getDouble("ciria.temperature"),
						result.getDouble("ciria.pmax"), result.getString("ciria.createdat"),
						result.getString("jobdetails.jobcode"), result.getString("jobdetails.cluster"),
						result.getString("jobdetails.jobname"), result.getString("jobdetails.joblocation"),
						result.getString("jobdetails.nameofstructure"), result.getString("jobdetails.nameofelement"),
						result.getInt("jobdetails.duration"), result.getString("users.username"),
						result.getInt("users.role")));
			}
		} catch (Exception e) {
			throw new CIRIAException("DATABASE BASE EXCEPTION" + e);
		}
		return ciriadetails;
	}

	/*public List<CIRIABean> getACIDetailfromDB(CIRIABean aci) throws CIRIAException {
		List<CIRIABean> ciriadetails = new ArrayList<CIRIABean>();
		try {
			String sql = "SELECT ciria.id,ciria.jobid,ciria.userid,ciria.formulaversion,ciria.typeofelement,ciria.cementtype,ciria.density,ciria.verticalformheight,ciria.verticalpourheight,ciria.planarea,ciria.latitude,ciria.longtitude,ciria.temperature,ciria.pmax,ciria.volume,ciria.createdat,jobdetails.jobid,jobdetails.jobcode,jobdetails.userid,jobdetails.cluster,jobdetails.jobname,jobdetails.joblocation,jobdetails.nameofstructure,jobdetails.nameofelement,jobdetails.duration,users.userid,users.name,users.branch,users.username,users.role FROM ciria INNER JOIN jobdetails ON ciria.jobid=jobdetails.jobid INNER JOIN users ON users.userid=users.userid where ciria.userid=?";
			connection = DBUtil.getDBConnection();
			stmt = connection.prepareStatement(sql);
			stmt.setInt(1, aci.getCiriaID());
			result = stmt.executeQuery();
			while (result.next()) {
				ciriadetails.add(new CIRIABean(result.getInt("ciria.id"), result.getInt("ciria.jobid"),
						result.getInt("ciria.userid"), result.getString("ciria.latitude"),
						result.getString("ciria.longtitude"), result.getString("ciria.formulaversion"),
						result.getDouble("ciria.typeofelement"), result.getDouble("ciria.cementtype"),
						result.getDouble("ciria.density"), result.getDouble("ciria.verticalformheight"),
						result.getDouble("ciria.verticalpourheight"), result.getDouble("ciria.volume"),
						result.getDouble("ciria.planarea"), result.getDouble("ciria.temperature"),
						result.getDouble("ciria.pmax"), result.getString("ciria.createdat"),
						result.getString("jobdetails.jobcode"), result.getString("jobdetails.cluster"),
						result.getString("jobdetails.jobname"), result.getString("jobdetails.joblocation"),
						result.getString("jobdetails.nameofstructure"), result.getString("jobdetails.nameofelement"),
						result.getInt("jobdetails.duration"), result.getString("users.name"),
						result.getString("users.branch"), result.getString("users.username"),
						result.getInt("users.role")));
			}
		} catch (Exception e) {
			throw new CIRIAException("DATABASE BASE EXCEPTION" + e);
		}
		return ciriadetails;
	}*/

	public boolean CIRIAidinDB(int CIRIAID) throws CIRIAException {
		boolean status = true;
		try {
			String sql = "SELECT id FROM ciria WHERE id=?";
			Connection connection = DBUtil.getDBConnection();
			PreparedStatement stmt = connection.prepareStatement(sql);
			stmt.setInt(1, CIRIAID);
			ResultSet result = stmt.executeQuery();
			if (result.next()) {
				status = false;
			}
		} catch (Exception e) {
			throw new CIRIAException("DATABASE BASE EXCEPTION");
		}
		return status;
	}

	public boolean CIRIAIDforUser(int CIRIAID, int userID) throws CIRIAException {
		boolean status = true;
		try {
			String sql = "SELECT id FROM ciria WHERE id=? and userid=?";
			Connection connection = DBUtil.getDBConnection();
			PreparedStatement stmt = connection.prepareStatement(sql);
			stmt.setInt(1, CIRIAID);
			stmt.setInt(2, userID);
			ResultSet result = stmt.executeQuery();
			if (result.next()) {
				status = false;
			}
		} catch (Exception e) {
			throw new CIRIAException("DATABASE BASE EXCEPTION");
		}
		return status;
	}

}
