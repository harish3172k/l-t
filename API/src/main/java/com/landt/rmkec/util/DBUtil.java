package com.landt.rmkec.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DBUtil {
	public static Connection connection;

	private static String jdbcUrl = "jdbc:mysql://rmk-landt.cqh5tsnrn3ol.us-east-2.rds.amazonaws.com/land_db";
	private static String username = "root";
	private static String password = "rmkcse104";

	public static Connection getDBConnection() {
		if (connection == null) {
			try {
				Class.forName("com.mysql.cj.jdbc.Driver");
				connection = DriverManager.getConnection(jdbcUrl, username, password);
			} catch (Exception e) {
				System.out.println(e.toString());
			}
		}
		
		return connection;
	}

	public static void closeDBConnection() {
		if (connection != null) {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
}