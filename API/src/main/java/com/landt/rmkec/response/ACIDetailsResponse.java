package com.landt.rmkec.response;

import java.util.List;

import com.landt.rmkec.beans.ACIBean;
import com.landt.rmkec.beans.JobDetailsBean;

public class ACIDetailsResponse {
	public int count = 0;
	public List<ACIBean> data;

	public ACIDetailsResponse(int count, List<ACIBean> data ) {
		super();
		this.count = count;
		this.data = data;
	}
	public ACIDetailsResponse() {
		super();
		// TODO Auto-generated constructor stub
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public List<ACIBean> getData() {
		return data;
	}

	public void setData(List<ACIBean> data) {
		this.data = data;
	}

}
