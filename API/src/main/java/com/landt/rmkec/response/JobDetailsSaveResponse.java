package com.landt.rmkec.response;

public class JobDetailsSaveResponse {

	public int jobID;

	public JobDetailsSaveResponse(int jobID) {
		super();
		this.jobID = jobID;
	}

	public JobDetailsSaveResponse() {
		super();
		
	}

	public int getJobID() {
		return jobID;
	}

	public void setJobID(int jobID) {
		this.jobID = jobID;
	}

	

}
