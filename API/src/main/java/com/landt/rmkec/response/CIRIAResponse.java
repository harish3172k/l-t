package com.landt.rmkec.response;

public class CIRIAResponse {
	int ciriaID;
	double temperature;
	double pmax;

	public CIRIAResponse(int ciriaID, double temperature, double pmax) {
		super();
		this.ciriaID = ciriaID;
		this.temperature = temperature;
		this.pmax = pmax;
	}

	public CIRIAResponse() {
		super();
		// TODO Auto-generated constructor stub
	}

	public int getCiriaID() {
		return ciriaID;
	}

	public void setCiriaID(int ciriaID) {
		this.ciriaID = ciriaID;
	}

	public double getTemperature() {
		return temperature;
	}

	public void setTemperature(int temperature) {
		this.temperature = temperature;
	}

	public double getPmax() {
		return pmax;
	}

	public void setPmax(double pmax) {
		this.pmax = pmax;
	}

}
