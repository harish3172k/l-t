package com.landt.rmkec.response;

public class ACIResponse {
	int aciID;
	double temperature;
	double ccpmax;

	public ACIResponse(int aciID, double temperature, double ccpmax) {
		super();
		this.aciID = aciID;
		this.temperature = temperature;
		this.ccpmax = ccpmax;
	}

	public ACIResponse() {
		super();
	}

	public int getAciID() {
		return aciID;
	}

	public void setAciID(int aciID) {
		this.aciID = aciID;
	}

	public double getTemperature() {
		return temperature;
	}

	public void setTemperature(int temperature) {
		this.temperature = temperature;
	}

	public double getCcpmax() {
		return ccpmax;
	}

	public void setCcpmax(double ccpmax) {
		this.ccpmax = ccpmax;
	}
	
	

}
