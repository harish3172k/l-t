package com.landt.rmkec.response;

public class UserLoginResponse {
	public int userID;
	public int roleID;

	public int getRoleID() {
		return roleID;
	}

	public void setRoleID(int roleID) {
		this.roleID = roleID;
	}

	public UserLoginResponse() {
		super();
	}

	public UserLoginResponse(int userid,int roleid) {
		super();
		this.userID = userid;
		this.roleID = roleid;
	}

	public int getUserID() {
		return userID;
	}

	public void setUserID(int userID) {
		this.userID = userID;
	}
}
