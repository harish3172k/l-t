package com.landt.rmkec.response;

import com.landt.rmkec.service.Main;

public class OpenWeatherResponse {
	private Main main;

	public Main getMain() {
		return main;
	}

	public void setMain(Main main) {
		this.main = main;
	}
}
