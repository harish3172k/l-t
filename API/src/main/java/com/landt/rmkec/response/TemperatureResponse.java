package com.landt.rmkec.response;

public class TemperatureResponse {
	public double temperature;

	public double getTemperature() {
		return temperature;
	}

	public void setTemperature(double temperature) {
		this.temperature = temperature;
	}

	public TemperatureResponse(double temperature) {
		super();
		this.temperature = temperature;
	}
}
