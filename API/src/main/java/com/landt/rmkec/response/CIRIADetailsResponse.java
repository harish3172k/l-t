package com.landt.rmkec.response;

import java.util.List;

import com.landt.rmkec.beans.CIRIABean;

public class CIRIADetailsResponse {
	public int count = 0;
	public List<CIRIABean> data;

	public CIRIADetailsResponse(int count, List<CIRIABean> data) {
		super();
		this.count = count;
		this.data = data;
	}

	public CIRIADetailsResponse() {
		super();
		// TODO Auto-generated constructor stub
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public List<CIRIABean> getData() {
		return data;
	}

	public void setData(List<CIRIABean> data) {
		this.data = data;
	}

}
