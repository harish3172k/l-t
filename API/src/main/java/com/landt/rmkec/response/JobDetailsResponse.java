package com.landt.rmkec.response;

import java.util.List;

import com.landt.rmkec.beans.JobDetailsBean;

public class JobDetailsResponse {

	public int count = 0;
	public List<JobDetailsBean> data;

	public List<JobDetailsBean> getData() {
		return data;
	}

	public void setData(List<JobDetailsBean> data) {
		this.data = data;
	}

	public JobDetailsResponse(int count, List<JobDetailsBean> data) {
		super();
		this.count = count;
		this.data = data;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

}
