package com.landt.rmkec.beans;

import java.io.Serializable;

public class UserLoginBean implements Serializable {
	private static final long serialVersionUID = 1L;

	public String UserName;
	public String Password;
	public int Role;

	public UserLoginBean() {
		super();
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public UserLoginBean(String username, String password, int role) {
		super();
		this.UserName = username;
		this.Password = password;
		this.Role = role;
	}

	public String getUserName() {
		return UserName;
	}

	public void setUserName(String userName) {
		UserName = userName;
	}

	public String getPassword() {
		return Password;
	}

	public void setPassword(String password) {
		Password = password;
	}

	public int getRole() {
		return Role;
	}

	public void setRole(int role) {
		Role = role;
	}
}
