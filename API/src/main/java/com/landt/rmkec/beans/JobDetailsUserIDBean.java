package com.landt.rmkec.beans;

import java.io.Serializable;

public class JobDetailsUserIDBean implements Serializable {
	private static final long serialVersionUID = 1L;
	public int userID;

	public int getUserID() {
		return userID;
	}

	public void setUserID(int userID) {
		this.userID = userID;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public JobDetailsUserIDBean(int userID) {
		super();
		this.userID = userID;
	}

	public JobDetailsUserIDBean() {
		super();
	}

}
