/**
 * 
 */
package com.landt.rmkec.beans;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

public class ACIBean implements Serializable {

	private static final long serialVersionUID = 1L;
	int aciID = 0;
	int jobID = 0;
	int userId = 0;
	String elementType;
	String cementType;
	String internalVibration;
	String slag;
	String flyash;
	public String retarders;
	String slump;
	String wallheight;
	String latitude;
	String longtiude;
	String createdON;
	double den;
	double volume;
	double verticalformheight;
	double ratee;
	double temperatureee = 0.0;
	double pmax = 0.0;
	// Job Details
	public String jobCode;
	public int userID;
	public String cluster;
	public String jobName;
	public String jobLocation;
	public String nameOfStructure;
	public String nameofElement;
	public int duration;
	//userdetails
	String name;
	String branch;
	String userName;
	int role;

	public static long getSerialversionuid() {
		return serialVersionUID;
	}


	public ACIBean(int aciID, int jobid, int userId, String elementType, String cementType,
			String internalVibration, String slag, String flyash, String retarders, String slump, String wallheight,
			String latitude,String longtiude, double den, double volume, double verticalformheight,
			double ratee, double temperatureee, double pmax, String createdON, String jobcode, String cluster, String jobName,
			String jobLocation, String nameOfStructure, String nameofElement, int duration,
			String userName, int role) {
		super();
		this.aciID = aciID;
		this.jobID = jobid;
		this.userId = userId;
		this.elementType = elementType;
		this.cementType = cementType;
		this.internalVibration = internalVibration;
		this.slag = slag;
		this.flyash = flyash;
		this.retarders = retarders;
		this.slump = slump;
		this.wallheight = wallheight;
		this.latitude = latitude;
		this.longtiude = longtiude;
		this.createdON = createdON;
		this.den = den;
		this.volume = volume;
		this.verticalformheight = verticalformheight;
		this.ratee = ratee;
		this.temperatureee = temperatureee;
		this.pmax = pmax;
		this.jobCode = jobcode;
		this.cluster = cluster;
		this.jobName = jobName;
		this.jobLocation = jobLocation;
		this.nameOfStructure = nameOfStructure;
		this.nameofElement = nameofElement;
		this.duration = duration;
		this.userName = userName;
		this.role = role;
	}


	public int getAciID() {
		return aciID;
	}

	public void setAciID(int aciID) {
		this.aciID = aciID;
	}

	public String getJobCode() {
		return jobCode;
	}

	public void setJobCode(String jobCode) {
		this.jobCode = jobCode;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String getElementType() {
		return elementType;
	}

	public void setElementType(String elementType) {
		this.elementType = elementType;
	}

	public String getCementType() {
		return cementType;
	}

	public void setCementType(String cementType) {
		this.cementType = cementType;
	}

	public String getInternalVibration() {
		return internalVibration;
	}

	public void setInternalVibration(String internalVibration) {
		this.internalVibration = internalVibration;
	}

	public String getSlag() {
		return slag;
	}

	public void setSlag(String slag) {
		this.slag = slag;
	}

	public String getFlyash() {
		return flyash;
	}

	public void setFlyash(String flyash) {
		this.flyash = flyash;
	}

	public String getRetarders() {
		return retarders;
	}

	public void setRetarders(String retarders) {
		this.retarders = retarders;
	}

	public String getSlump() {
		return slump;
	}

	public void setSlump(String slump) {
		this.slump = slump;
	}

	public String getWallheight() {
		return wallheight;
	}

	public void setWallheight(String wallheight) {
		this.wallheight = wallheight;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getLongtiude() {
		return longtiude;
	}

	public void setLongtiude(String longtiude) {
		this.longtiude = longtiude;
	}

	public double getDen() {
		return den;
	}

	public void setDen(double den) {
		this.den = den;
	}

	public double getVolume() {
		return volume;
	}

	public void setVolume(double volume) {
		this.volume = volume;
	}

	public double getVerticalformheight() {
		return verticalformheight;
	}

	public void setVerticalformheight(double verticalformheight) {
		this.verticalformheight = verticalformheight;
	}

	public double getRatee() {
		return ratee;
	}

	public void setRatee(double ratee) {
		this.ratee = ratee;
	}

	public double getTemperatureee() {
		return temperatureee;
	}

	public void setTemperatureee(double temperatureee) {
		this.temperatureee = temperatureee;
	}

	public double getPmax() {
		return pmax;
	}

	public void setPmax(double pmax) {
		this.pmax = pmax;
	}

	public String getCreatedON() {
		return createdON;
	}

	public void setCreatedON(String createdON) {
		this.createdON = createdON;
	}

	public int getJobID() {
		return jobID;
	}

	public void setJobID(int jobID) {
		this.jobID = jobID;
	}

	public String getJobcode() {
		return jobCode;
	}

	public void setJobcode(String jobcode) {
		this.jobCode = jobcode;
	}

	public int getUserID() {
		return userID;
	}

	public void setUserID(int userID) {
		this.userID = userID;
	}

	public String getCluster() {
		return cluster;
	}

	public void setCluster(String cluster) {
		this.cluster = cluster;
	}

	public String getJobName() {
		return jobName;
	}

	public void setJobName(String jobName) {
		this.jobName = jobName;
	}

	public String getJobLocation() {
		return jobLocation;
	}

	public void setJobLocation(String jobLocation) {
		this.jobLocation = jobLocation;
	}

	public String getNameOfStructure() {
		return nameOfStructure;
	}

	public void setNameOfStructure(String nameOfStructure) {
		this.nameOfStructure = nameOfStructure;
	}

	public String getNameofElement() {
		return nameofElement;
	}

	public void setNameofElement(String nameofElement) {
		this.nameofElement = nameofElement;
	}

	public int getDuration() {
		return duration;
	}

	public void setDuration(int duration) {
		this.duration = duration;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getBranch() {
		return branch;
	}

	public void setBranch(String branch) {
		this.branch = branch;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public int getRole() {
		return role;
	}

	public void setRole(int role) {
		this.role = role;
	}

}
