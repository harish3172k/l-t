package com.landt.rmkec.beans;

public class CIRIABean {

	private static final long serialVersionUID = 1L;
	int ciriaID = 0;
	int jobID = 0;
	int userId = 0;
	String latitude = "";
	String longtitude = "";
	double elementt;
	double c2;
	double density;
	double verticalFormHeight;
	double verticalPourHeight;
	double volume;
	double rate;
	double temperature = 0.0;
	double pmax = 0.0;
	String createdON;
	// Job Details
	public String jobcode;
	public String cluster;
	public String jobName;
	public String jobLocation;
	public String nameOfStructure;
	public String nameofElement;
	public int duration;
	// userdetails
	String name;
	String branch;
	String userName;
	int role;

	public CIRIABean() {
		super();
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public CIRIABean(int id, int jobID, int userId, String latitude, String longtitude, 
			double elementt, double c2, double density, double verticalFormHeight, double verticalPourHeight,
			double volume, double rate, double temperature, double pmax, String createdon) {
		super();
		this.ciriaID = id;
		this.jobID = jobID;
		this.userId = userId;
		this.latitude = latitude;
		this.longtitude = longtitude;
		this.elementt = elementt;
		this.c2 = c2;
		this.density = density;
		this.verticalFormHeight = verticalFormHeight;
		this.verticalPourHeight = verticalPourHeight;
		this.volume = volume;
		this.rate = rate;
		this.temperature = temperature;
		this.pmax = pmax;
		this.createdON = createdon;
	}

	public CIRIABean(int ciriaID, int jobID, int userId, String latitude, String longtitude, 
			double elementt, double c2, double density, double verticalFormHeight, double verticalPourHeight,
			double volume, double rate, double temperature, double pmax, String createdON, String jobcode,
			String cluster, String jobName, String jobLocation, String nameOfStructure, String nameofElement,
			int duration, String userName, int role) {
		super();
		this.ciriaID = ciriaID;
		this.jobID = jobID;
		this.userId = userId;
		this.latitude = latitude;
		this.longtitude = longtitude;
		this.elementt = elementt;
		this.c2 = c2;
		this.density = density;
		this.verticalFormHeight = verticalFormHeight;
		this.verticalPourHeight = verticalPourHeight;
		this.volume = volume;
		this.rate = rate;
		this.temperature = temperature;
		this.pmax = pmax;
		this.createdON = createdON;
		this.jobcode = jobcode;
		this.cluster = cluster;
		this.jobName = jobName;
		this.jobLocation = jobLocation;
		this.nameOfStructure = nameOfStructure;
		this.nameofElement = nameofElement;
		this.duration = duration;
		this.userName = userName;
		this.role = role;
	}

	public int getCiriaID() {
		return ciriaID;
	}

	public void setCiriaID(int ciriaID) {
		this.ciriaID = ciriaID;
	}

	public int getJobID() {
		return jobID;
	}

	public void setJobID(int jobID) {
		this.jobID = jobID;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getLongtitude() {
		return longtitude;
	}

	public void setLongtitude(String longtitude) {
		this.longtitude = longtitude;
	}



	public double getElementt() {
		return elementt;
	}

	public void setElementt(double elementt) {
		this.elementt = elementt;
	}

	public double getC2() {
		return c2;
	}

	public void setC2(double c2) {
		this.c2 = c2;
	}

	public double getDensity() {
		return density;
	}

	public void setDensity(double density) {
		this.density = density;
	}

	public double getVerticalFormHeight() {
		return verticalFormHeight;
	}

	public void setVerticalFormHeight(double verticalFormHeight) {
		this.verticalFormHeight = verticalFormHeight;
	}

	public double getVerticalPourHeight() {
		return verticalPourHeight;
	}

	public void setVerticalPourHeight(double verticalPourHeight) {
		this.verticalPourHeight = verticalPourHeight;
	}

	public double getVolume() {
		return volume;
	}

	public void setVolume(double volume) {
		this.volume = volume;
	}

	public double getRate() {
		return rate;
	}

	public void setRate(double rate) {
		this.rate = rate;
	}

	public double getTemperature() {
		return temperature;
	}

	public void setTemperature(double temperature) {
		this.temperature = temperature;
	}

	public double getPmax() {
		return pmax;
	}

	public void setPmax(double pmax) {
		this.pmax = pmax;
	}

	public String getCreatedON() {
		return createdON;
	}

	public void setCreatedON(String createdON) {
		this.createdON = createdON;
	}

	public String getJobcode() {
		return jobcode;
	}

	public void setJobcode(String jobcode) {
		this.jobcode = jobcode;
	}

	public int getUserID() {
		return userId;
	}

	public void setUserID(int userID) {
		this.userId = userID;
	}

	public String getCluster() {
		return cluster;
	}

	public void setCluster(String cluster) {
		this.cluster = cluster;
	}

	public String getJobName() {
		return jobName;
	}

	public void setJobName(String jobName) {
		this.jobName = jobName;
	}

	public String getJobLocation() {
		return jobLocation;
	}

	public void setJobLocation(String jobLocation) {
		this.jobLocation = jobLocation;
	}

	public String getNameOfStructure() {
		return nameOfStructure;
	}

	public void setNameOfStructure(String nameOfStructure) {
		this.nameOfStructure = nameOfStructure;
	}

	public String getNameofElement() {
		return nameofElement;
	}

	public void setNameofElement(String nameofElement) {
		this.nameofElement = nameofElement;
	}

	public int getDuration() {
		return duration;
	}

	public void setDuration(int duration) {
		this.duration = duration;
	}




	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public int getRole() {
		return role;
	}

	public void setRole(int role) {
		this.role = role;
	}
}
