package com.landt.rmkec.beans;

public class TemperatureBean {
	public static float MIN_LATITUDE = Float.valueOf("-90.0000");
	public static float MAX_LATITUDE = Float.valueOf("90.0000");
	public static float MIN_LONGITUDE = Float.valueOf("-180.0000");
	public static float MAX_LONGITUDE = Float.valueOf("180.0000");
	public static float EARTH_DIAMETER = Float.valueOf("12756.274");
	private float latitude;
	private float longitude;

	public TemperatureBean(float latitude, float longitude) {

		if (isValidLatitude(latitude) == true && isValidLongitude(longitude) == true) {
			this.latitude = latitude;
			this.longitude = longitude;
		} else {
			throw new IllegalArgumentException(
					"The parameters did not pass validation as defined by the CoordinateManager class");
		}
	}

	public float getLatitude() {
		return latitude;
	}

	public void setLatitude(float latitude) {
		this.latitude = latitude;
	}

	public float getLongitude() {
		return longitude;
	}

	public void setLongitude(float longitude) {
		this.longitude = longitude;
	}

	protected static boolean isValidLatitude(float latitude) {
		if (latitude >= MIN_LATITUDE && latitude <= MAX_LATITUDE) {
			return true;
		} else {
			return false;
		}
	}

	public static boolean isValidLongitude(float longitude) {
		if (longitude >= MIN_LONGITUDE && longitude <= MAX_LONGITUDE) {
			return true;
		} else {
			return false;
		}
	}

}
