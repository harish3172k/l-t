package com.landt.rmkec.beans;

import java.io.Serializable;

public class JobDetailsBean implements Serializable {
	private static final long serialVersionUID = 1L;
	public int jobID;
	public String jobcode;
	public int userID;
	public String cluster;
	public String jobName;
	public String jobLocation;
	public String nameOfStructure;
	public String nameofElement;
	public int duration;
	public String createdAt;

	public JobDetailsBean() {
		super();
	}

	
	// to store job information
	public JobDetailsBean(int jobid, String jobcode, int userID, String cluster, String jobName, String jobLocation,
			String nameOfStructure, String nameofElement, int duration,String timestamp) {
		super();
		this.jobID = jobid;
		this.jobcode = jobcode;
		this.userID = userID;
		this.cluster = cluster;
		this.jobName = jobName;
		this.jobLocation = jobLocation;
		this.nameOfStructure = nameOfStructure;
		this.nameofElement = nameofElement;
		this.duration = duration;
		this.createdAt=timestamp;
	}

	public int getUserID() {
		return userID;
	}

	public void setUserID(int userID) {
		this.userID = userID;
	}

	public String getCluster() {
		return cluster;
	}

	public void setCluster(String cluster) {
		this.cluster = cluster;
	}

	public String getJobName() {
		return jobName;
	}

	public void setJobName(String jobName) {
		this.jobName = jobName;
	}

	public String getJobLocation() {
		return jobLocation;
	}

	public void setJobLocation(String jobLocation) {
		this.jobLocation = jobLocation;
	}

	public String getNameOfStructure() {
		return nameOfStructure;
	}

	public void setNameOfStructure(String nameOfStructure) {
		this.nameOfStructure = nameOfStructure;
	}

	public String getNameofElement() {
		return nameofElement;
	}

	public void setNameofElement(String nameofElement) {
		this.nameofElement = nameofElement;
	}

	public int getDuration() {
		return duration;
	}

	public void setDuration(int duration) {
		this.duration = duration;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getJobcode() {
		return jobcode;
	}

	public void setJobcode(String jobcode) {
		this.jobcode = jobcode;
	}

	public int getJobID() {
		return jobID;
	}

	public void setJobID(int jobID) {
		this.jobID = jobID;
	}


	public String getCreatedAt() {
		return createdAt;
	}


	public void setCreatedAt(String createdAt) {
		this.createdAt = createdAt;
	}
	
	

}
