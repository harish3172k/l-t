package com.landt.rmkec.beans;

public class UserDetailBean {

	int userID;
	String name;
	String branch;
	String userName;
	int role;

	public UserDetailBean() {
		super();
		// TODO Auto-generated constructor stub
	}

	public UserDetailBean(int userID, String name, String branch, String userName, int role) {
		super();
		this.userID = userID;
		this.name = name;
		this.branch = branch;
		this.userName = userName;
		this.role = role;
	}

	public int getUserID() {
		return userID;
	}

	public void setUserID(int userID) {
		this.userID = userID;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getBranch() {
		return branch;
	}

	public void setBranch(String branch) {
		this.branch = branch;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public int getRole() {
		return role;
	}

	public void setRole(int role) {
		this.role = role;
	}

}
