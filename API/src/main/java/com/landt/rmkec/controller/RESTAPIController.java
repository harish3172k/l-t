package com.landt.rmkec.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.landt.rmkec.beans.ACIBean;
import com.landt.rmkec.beans.CIRIABean;
import com.landt.rmkec.beans.JobDetailsBean;
import com.landt.rmkec.beans.JobDetailsUserIDBean;
import com.landt.rmkec.beans.TemperatureBean;
import com.landt.rmkec.beans.UserLoginBean;
import com.landt.rmkec.exception.ACIException;
import com.landt.rmkec.exception.CIRIAException;
import com.landt.rmkec.exception.JobDetailsException;
import com.landt.rmkec.exception.TemperatureNotFoundException;
import com.landt.rmkec.exception.UserLoginException;
import com.landt.rmkec.response.ACIDetailsResponse;
import com.landt.rmkec.response.ACIResponse;
import com.landt.rmkec.response.CIRIADetailsResponse;
import com.landt.rmkec.response.CIRIAResponse;
import com.landt.rmkec.response.JobDetailsResponse;
import com.landt.rmkec.response.JobDetailsSaveResponse;
import com.landt.rmkec.response.OpenWeatherResponse;
import com.landt.rmkec.response.TemperatureResponse;
import com.landt.rmkec.response.UserLoginResponse;
import com.landt.rmkec.service.ACIService;
import com.landt.rmkec.service.CIRIAService;
import com.landt.rmkec.service.JobDetailService;
import com.landt.rmkec.service.UserLoginService;

@RestController
public class RESTAPIController {

	private String apikey = "b489d6cf97230d699a36c1b850e5e84f";

	// ACI formula Calculation
	@PostMapping(path = "/aci", consumes = "application/json", produces = "application/json")
	ResponseEntity<?> calculatepressureaci(@RequestBody ACIBean aciWallsData)
			throws ACIException, TemperatureNotFoundException, JobDetailsException {
		ACIResponse response = new ACIService().ACIServiceCalculate(aciWallsData);
		if (response == null) {
			throw new ACIException("Please contact admin");
		}
		return new ResponseEntity<ACIResponse>(
				new ACIResponse(response.getAciID(), response.getTemperature(), response.getCcpmax()), HttpStatus.OK);
	}

	// CIRIA Formula Calculation
	@PostMapping(path = "/ciria", consumes = "application/json", produces = "application/json")
	ResponseEntity<?> calculatePressure(@RequestBody CIRIABean ciriaData)
			throws CIRIAException, TemperatureNotFoundException, JobDetailsException {
		CIRIAResponse pmax = new CIRIAService().CIRIAServiceCalculate(ciriaData);
		if (pmax == null) {
			throw new CIRIAException("Please contact admin");
		}
		return new ResponseEntity<CIRIAResponse>(
				new CIRIAResponse(pmax.getCiriaID(), pmax.getTemperature(), pmax.getPmax()), HttpStatus.OK);
	}

	// User Login Check
	@PostMapping(path = "/userlogin", consumes = "application/json", produces = "application/json")
	ResponseEntity<?> UserLogin(@Valid @RequestBody UserLoginBean userData) throws UserLoginException {
		int userID = new UserLoginService().LoginCheck(userData);
		if (userID <= 0)
			throw new UserLoginException("Cant able login. Contact Admin");
		else
			return new ResponseEntity<UserLoginResponse>(new UserLoginResponse(userID, userData.getRole()),
					HttpStatus.OK);
	}

	// Post Job Details
	@PostMapping(path = "/postjobdetails", consumes = "application/json", produces = "application/json")
	ResponseEntity<?> postjobdetails(@RequestBody JobDetailsBean jobDetailsPostData) throws JobDetailsException {
		int jobid = new JobDetailService().saveJobDetails(jobDetailsPostData);
		return new ResponseEntity<JobDetailsSaveResponse>(new JobDetailsSaveResponse(jobid), HttpStatus.OK);
	}

	// Get Job Details
	@PostMapping(path = "/getjobdetailsofuser", consumes = "application/json", produces = "application/json")
	ResponseEntity<?> fetchjobs(@RequestBody JobDetailsUserIDBean jobDetailUserID) throws JobDetailsException {
		List<JobDetailsBean> jobDetailsFromDatabase = new JobDetailService().getJobDetailsOfAUser(jobDetailUserID);
		return new ResponseEntity<JobDetailsResponse>(
				new JobDetailsResponse(jobDetailsFromDatabase.size(), jobDetailsFromDatabase), HttpStatus.OK);
	}

	@PostMapping(path = "/temperature", consumes = "application/json", produces = "application/json")
	ResponseEntity<?> getTemperaturebyLocationCoordinates(@RequestBody TemperatureBean tempBean) {
		RestTemplate restTemplate = new RestTemplate();
		if(tempBean.getLatitude()==0.0&&tempBean.getLongitude()==0.0)
		{
			final double temp=0.0;
			return new ResponseEntity<TemperatureResponse>(new TemperatureResponse(temp), HttpStatus.OK);
		}
		else
		{
		final String url = "https://api.openweathermap.org/data/2.5/weather?lat=" + tempBean.getLatitude() + "&lon="
				+ tempBean.getLongitude() + "&APPID=" + apikey + "&units=metric";
		ResponseEntity<OpenWeatherResponse> response = restTemplate.getForEntity(url, OpenWeatherResponse.class);
		final double temp = response.getBody().getMain().getTemp();
		return new ResponseEntity<TemperatureResponse>(new TemperatureResponse(temp), HttpStatus.OK);
	}}

	// get all aci details of a user
	@PostMapping(path = "/getallacidetailsofauser", consumes = "application/json", produces = "application/json")
	ResponseEntity<?> fetchACIDetails(@RequestBody ACIBean acilUserID) throws JobDetailsException, ACIException {
		List<ACIBean> allACIDetailsofaUser = new ACIService().getallACIDetailsOfAUser(acilUserID);
		return new ResponseEntity<ACIDetailsResponse>(
				new ACIDetailsResponse(allACIDetailsofaUser.size(), allACIDetailsofaUser), HttpStatus.OK);
	}

	/*
	 * get aci detail
	 * 
	 * @PostMapping(path = "/getacidetail", consumes = "application/json", produces
	 * = "application/json") ResponseEntity<?> fetchACIDetailbyID(@RequestBody
	 * ACIBean acilID) throws JobDetailsException, ACIException { List<ACIBean>
	 * allACIDetail = new ACIService().getACIDetailbyID(acilID); return new
	 * ResponseEntity<ACIDetailsResponse>(new
	 * ACIDetailsResponse(allACIDetail.size(), allACIDetail), HttpStatus.OK); }
	 */

	// get all ciria details of a user
	@PostMapping(path = "/getallciriadetailsofauser", consumes = "application/json", produces = "application/json")
	ResponseEntity<?> fetchCIRIADetails(@RequestBody CIRIABean cirialUserID)
			throws JobDetailsException, CIRIAException {
		List<CIRIABean> allCIRIADetailsofaUser = new CIRIAService().getallCIRIADetailsOfAUser(cirialUserID);
		return new ResponseEntity<CIRIADetailsResponse>(
				new CIRIADetailsResponse(allCIRIADetailsofaUser.size(), allCIRIADetailsofaUser), HttpStatus.OK);
	}

	/*
	 * get ciria detail
	 * 
	 * @PostMapping(path = "/getciriadetail", consumes = "application/json",
	 * produces = "application/json") ResponseEntity<?>
	 * fetchCIRIADetailbyID(@RequestBody CIRIABean ciriaID) throws
	 * JobDetailsException, CIRIAException { List<CIRIABean> allACIDetail = new
	 * CIRIAService().getCIRIADetailbyID(ciriaID); return new
	 * ResponseEntity<CIRIADetailsResponse>(new
	 * CIRIADetailsResponse(allACIDetail.size(), allACIDetail), HttpStatus.OK); }
	 */

}