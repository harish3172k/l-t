package com.landt.rmkec.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class EmployeeController
{
	
	@RequestMapping(value="/", method = RequestMethod.GET)
    public String showLoginPage(ModelMap model){
        return "index";
    }
	
	@RequestMapping(value="/web/login", method = RequestMethod.POST)
    public String webLoginPage(ModelMap model){
		
        return "dashboard";
    }
	
}