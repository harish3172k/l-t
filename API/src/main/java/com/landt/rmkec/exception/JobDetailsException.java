package com.landt.rmkec.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class JobDetailsException extends Exception {
	private static final long serialVersionUID = 1L;
	private String errorMessage;

	public String getErrorMessage() {
		return errorMessage;
	}

	public JobDetailsException(String errorMessage) {
		super(errorMessage);
		this.errorMessage = errorMessage;
	}

	public JobDetailsException() {
		super();
	}

	@Override
	public synchronized Throwable fillInStackTrace() {
		return this;
	}

}
