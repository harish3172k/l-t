package com.landt.rmkec.exception;


public class ACIException extends Exception {

	private static final long serialVersionUID = 1L;
	private String errorMessage;

	public String getErrorMessage() {
		return errorMessage;
	}

	public ACIException(String errorMessage) {
		super(errorMessage);
		this.errorMessage = errorMessage;
	}

	public ACIException() {
		super();
	}

	@Override
	public synchronized Throwable fillInStackTrace() {
		return this;
	}

}
