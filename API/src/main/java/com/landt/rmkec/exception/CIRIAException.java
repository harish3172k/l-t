package com.landt.rmkec.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

public class CIRIAException extends Exception {

	private static final long serialVersionUID = 1L;
	private String errorMessage;

	public String getErrorMessage() {
		return errorMessage;
	}

	public CIRIAException(String errorMessage) {
		super(errorMessage);
		this.errorMessage = errorMessage;
	}

	public CIRIAException() {
		super();
	}

	@Override
	public synchronized Throwable fillInStackTrace() {
		return this;
	}

}
