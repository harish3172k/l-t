package com.landt.rmkec.exception;

import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.http.HttpStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class UserLoginException extends RuntimeException {
	private static final long serialVersionUID = 1L;
	private String errorMessage;

	public String getErrorMessage() {
		return errorMessage;
	}

	public UserLoginException(String errorMessage) {
		super(errorMessage);
		this.errorMessage = errorMessage;
	}

	public UserLoginException() {
		super();
	}

	@Override
	public synchronized Throwable fillInStackTrace() {
		return this;
	}
}
