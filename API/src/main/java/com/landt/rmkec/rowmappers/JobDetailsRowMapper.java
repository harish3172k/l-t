package com.landt.rmkec.rowmappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.landt.rmkec.beans.JobDetailsBean;

public class JobDetailsRowMapper implements RowMapper<JobDetailsBean> {
	@Override
	public JobDetailsBean mapRow(ResultSet row, int rowNum) throws SQLException {
		JobDetailsBean jobdetails = new JobDetailsBean();
		jobdetails.setJobID(row.getInt(1));
		jobdetails.setJobcode(row.getString(2));
		jobdetails.setUserID(row.getInt(3));
		jobdetails.setCluster(row.getString(4));
		jobdetails.setJobName(row.getString(5));
		jobdetails.setJobLocation(row.getString(6));
		jobdetails.setNameOfStructure(row.getString(7));
		jobdetails.setNameofElement(row.getString(8));
		jobdetails.setDuration(row.getInt(9));
		return jobdetails;
	}
}
