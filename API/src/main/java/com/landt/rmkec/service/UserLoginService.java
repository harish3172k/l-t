package com.landt.rmkec.service;

import com.landt.rmkec.beans.UserLoginBean;
import com.landt.rmkec.dao.UserLoginDAO;
import com.landt.rmkec.exception.UserLoginException;

public class UserLoginService {

	public int userid;

	public int LoginCheck(UserLoginBean userlogindata) {
		if (userlogindata.getUserName().equals("") || userlogindata.getUserName() == null) {
			throw new UserLoginException("Users Name is Required");
		} else if (userlogindata.getPassword().equals("") || userlogindata.getPassword() == null) {
			throw new UserLoginException("Password is Required");
		} else if (userlogindata.getRole() <= 0) {
			throw new UserLoginException("Role is Required");
		}
		return new UserLoginDAO().CheckUserLogin(userlogindata);
	}
}
