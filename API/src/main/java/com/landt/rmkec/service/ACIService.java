package com.landt.rmkec.service;

import java.text.DecimalFormat;
import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import com.landt.rmkec.beans.ACIBean;
import com.landt.rmkec.beans.JobDetailsBean;
import com.landt.rmkec.beans.JobDetailsUserIDBean;
import com.landt.rmkec.dao.ACIDAO;
import com.landt.rmkec.dao.JobDetailsDAO;
import com.landt.rmkec.exception.ACIException;
import com.landt.rmkec.exception.JobDetailsException;
import com.landt.rmkec.exception.TemperatureNotFoundException;
import com.landt.rmkec.response.ACIResponse;
import com.landt.rmkec.response.OpenWeatherResponse;

public class ACIService {
	public String[] retardersInputs = { "Yes", "No" };
	public String[] cementTypeInputs = { "Type 1", "Type 2", "Type 3", "Any Type" };
	public String[] elementTypeInputs = { "WALLS", "COLUMNS", "ANY" };
	public String[] slagInputs = { "<70", "None", ">=70" };
	public String[] flyAshInputs = { "<40", "None", ">=40" };
	public String[] vibrationInputs = { "<=1.2", "Any Type", ">1.2" };
	public String[] slumpInputs = { ">175", "<=175", ">1.2" };
	public String[] wallHeightInputs = { ">4.2", "<=4.2", "Any Type" };
	public float MIN_LATITUDE = Float.valueOf("-90.0000");
	public float MAX_LATITUDE = Float.valueOf("90.0000");
	public float MIN_LONGITUDE = Float.valueOf("-180.0000");
	public float MAX_LONGITUDE = Float.valueOf("180.0000");

	public ACIResponse ACIServiceCalculate(ACIBean aciBeanData)
			throws TemperatureNotFoundException, ACIException, JobDetailsException {
		String twoDoublesRegularExpression = "-?[1-9][0-9]*(\\.[0-9]+)?,\\s*-?[1-9][0-9]*(\\.[0-9]+)?";
		if (aciBeanData.getUserId() < 0) {
			throw new ACIException("User ID cannot be null");
		}
		if (new JobDetailService().isUserinDatabase(aciBeanData.getUserId())) {
			throw new ACIException("User ID is not in Database");
		}
		if (aciBeanData.getJobID() < 0) {
			throw new ACIException("Job ID cannot be null");
		}
		if (new JobDetailService().isJobinDatabase(aciBeanData.getJobID())) {
			throw new ACIException("Job ID is not in Database");
		}
		if (!String.valueOf(aciBeanData.getTemperatureee()).matches("[0-9]{1,13}(\\.[0-9]*)?")) {
			throw new ACIException("Temperature is incorrect Format. Ex:00.00");
		}
		if (validateInput(retardersInputs, aciBeanData.getRetarders())) {
			throw new ACIException("Retarders data is incorrect");
		}
		if (validateInput(cementTypeInputs, aciBeanData.getCementType())) {
			throw new ACIException("Cement Type is Invalid");
		}
		if (validateInput(elementTypeInputs, aciBeanData.getElementType())) {
			throw new ACIException("Element Type is Invalid");
		}
		if (validateInput(slagInputs, aciBeanData.getSlag())) {
			throw new ACIException("Slag is Invalid");
		}
		if (validateInput(flyAshInputs, aciBeanData.getFlyash())) {
			throw new ACIException("Fly Ash is Invalid");
		}
		if (validateInput(vibrationInputs, aciBeanData.getInternalVibration())) {
			throw new ACIException("Internal Vibration is Invalid");
		}
		if (validateInput(slumpInputs, aciBeanData.getSlump())) {
			throw new ACIException("Internal Vibration is Invalid");
		}
		if (validateInput(wallHeightInputs, aciBeanData.getWallheight())) {
			throw new ACIException("Wall Height is Invalid");
		}
		if (!String.valueOf(aciBeanData.getVerticalformheight()).matches("[0-9]{1,13}(\\.[0-9]*)?")) {
			throw new ACIException("Vertical Form Height incorrect Format. Ex:00.00");
		}
		if (!String.valueOf(aciBeanData.getRatee()).matches("[0-9]{1,13}(\\.[0-9]*)?")) {
			throw new ACIException("Plan Area incorrect Format. Ex:00.00");
		}
		if (!String.valueOf(aciBeanData.getDen()).matches("[0-9]{1,13}(\\.[0-9]*)?")) {
			throw new ACIException("Density incorrect Format. Ex:00.00");
		}
		/*
		 * if (Double.valueOf(aciBeanData.getLatitude()) >= MIN_LATITUDE &&
		 * Double.valueOf(aciBeanData.getLongtiude()) <= MAX_LATITUDE) { throw new
		 * ACIException("Latitude incorrect Format"); }
		 */

		DecimalFormat df = new DecimalFormat("#.##");
		double R =aciBeanData.getRatee();
		double cc = 0;
		boolean flag = true;
		if (aciBeanData.getRetarders().equalsIgnoreCase("Yes")) {
			if ((aciBeanData.getCementType().equalsIgnoreCase("Type 1")
					|| (aciBeanData.getCementType().equalsIgnoreCase("Type 2"))
					|| (aciBeanData.getCementType()).equalsIgnoreCase("Type 3")
							&& (aciBeanData.getSlag()).equalsIgnoreCase("None")
							&& (aciBeanData.getFlyash().equalsIgnoreCase("None")))) {
				cc = 1.2;
			} else if ((aciBeanData.getCementType().equalsIgnoreCase("Any Type"))
					&& (aciBeanData.getSlag().equalsIgnoreCase("<70"))
					&& (aciBeanData.getFlyash().equalsIgnoreCase("<40"))) {
				cc = 1.4;
			} else if ((aciBeanData.getCementType().equalsIgnoreCase("Any Type"))
					&& (aciBeanData.getSlag().equalsIgnoreCase(">=70"))
					&& (aciBeanData.getFlyash().equalsIgnoreCase(">=40"))) {
				cc = 1.5;
			}
		} else if (aciBeanData.getRetarders().equalsIgnoreCase("No")) {
			if ((aciBeanData.getCementType().equalsIgnoreCase("Type 1")
					|| (aciBeanData.getCementType()).equalsIgnoreCase("Type 2"))
					|| (aciBeanData.getCementType().equalsIgnoreCase("Type 3"))

							&& (aciBeanData.getSlag()).equalsIgnoreCase("None")
							&& (aciBeanData.getFlyash()).equalsIgnoreCase("None")) {
				cc = 1.0;
			} else if ((aciBeanData.getCementType().equalsIgnoreCase("Any Type"))
					&& (aciBeanData.getSlag().equalsIgnoreCase("<70"))
					&& (aciBeanData.getFlyash().equalsIgnoreCase("<40"))) {
				cc = 1.2;
			} else if ((aciBeanData.getCementType().equalsIgnoreCase("Any Type"))
					&& (aciBeanData.getSlag().equalsIgnoreCase(">=70"))
					&& (aciBeanData.getFlyash().equalsIgnoreCase(">=40"))) {
				cc = 1.4;
			}
		}

		double cw = 0;

		if (aciBeanData.getDen() < 2240) {
			cw = 0.5 * (1 + (aciBeanData.getDen() / 2320));
			if (cw < 0.8) {
				cw = 0.8;
			}

		} else if ((aciBeanData.getDen() <= 2400) && (aciBeanData.getDen() >= 2240)) {
			cw = 1.0;

		} else if (aciBeanData.getDen() > 2400) {
			cw = (aciBeanData.getDen() / 2320);
		}
		//
		double pressure2 = 0;
		double ccpmax = 0;
		if (aciBeanData.getElementType().equalsIgnoreCase("ANY")) {
			if ((aciBeanData.getSlump().equalsIgnoreCase(">175"))
					&& (aciBeanData.getInternalVibration().equalsIgnoreCase("Any Type"))) {
				pressure2 = (aciBeanData.getDen() * 9.8 * aciBeanData.getVerticalformheight());
				pressure2 = pressure2 / 1000.00;
				ccpmax = pressure2;
				flag = false;
				aciBeanData.setPmax(ccpmax);
				int aciID = new ACIDAO().createRecordACI(aciBeanData);
				if (aciID > 0) {
					return new ACIResponse(aciID, aciBeanData.getTemperatureee(), Double.valueOf(df.format(ccpmax)));
				} else {
					throw new ACIException("Data's not saved to Database");
				}

			} else if ((aciBeanData.getSlump().equalsIgnoreCase("<=175"))
					&& (aciBeanData.getInternalVibration().equalsIgnoreCase(">1.2"))) {
				pressure2 = (aciBeanData.getDen() * 9.8 * aciBeanData.getVerticalformheight());
				pressure2 = pressure2 / 1000.00;
				ccpmax = pressure2;
				flag = false;
				aciBeanData.setPmax(ccpmax);
				int aciID = new ACIDAO().createRecordACI(aciBeanData);
				if (aciID > 0) {
					return new ACIResponse(aciID, aciBeanData.getTemperatureee(), Double.valueOf(df.format(ccpmax)));
				} else {
					throw new ACIException("Data's not saved to Database");
				}
			}
		} else if (aciBeanData.getElementType().equalsIgnoreCase("WALLS")) {
			if ((aciBeanData.getSlump().equalsIgnoreCase("<=175"))
					&& (aciBeanData.getInternalVibration().equalsIgnoreCase("<=1.2")) && (R >= 2.10) && (R <= 4.5)) {
				ccpmax = cc * cw * (7.2 + (1156 / (aciBeanData.getTemperatureee() + 17.8))
						+ ((244 * R) / (aciBeanData.getTemperatureee() + 17.8)));
			} else if ((aciBeanData.getSlump().equalsIgnoreCase("<=175"))
					&& (aciBeanData.getInternalVibration().equalsIgnoreCase("<=1.2")) && (R > 4.50)) {
				pressure2 = (aciBeanData.getDen() * 9.8 * aciBeanData.getVerticalformheight());
				pressure2 = pressure2 / 1000;
				ccpmax = pressure2;
				flag = false;
				aciBeanData.setPmax(ccpmax);
				int aciID = new ACIDAO().createRecordACI(aciBeanData);
				if (aciID > 0) {
					return new ACIResponse(aciID, aciBeanData.getTemperatureee(), Double.valueOf(df.format(ccpmax)));
				} else {
					throw new ACIException("Data's not saved to Database");
				}
			} else if ((aciBeanData.getWallheight().equalsIgnoreCase("<=4.2"))
					&& (aciBeanData.getSlump().equalsIgnoreCase("<=175"))
					&& (aciBeanData.getInternalVibration().equalsIgnoreCase("<=1.2")) && (R < 2.1)) {
				ccpmax = cc * cw * (7.2 + ((785 * R) / (aciBeanData.getTemperatureee() + 17.8)));
			} else if ((aciBeanData.getWallheight().equalsIgnoreCase(">4.2"))
					&& (aciBeanData.getSlump().equalsIgnoreCase("<=175"))
					&& (aciBeanData.getInternalVibration().equalsIgnoreCase("<=1.2")) && (R < 2.1)) {
				ccpmax = cc * cw * (7.2 + (1156 / (aciBeanData.getTemperatureee() + 17.8))
						+ ((244 * R) / (aciBeanData.getTemperatureee() + 17.8)));
			}
		} else if (aciBeanData.getElementType().equalsIgnoreCase("COLUMNS")) {
			if ((aciBeanData.getSlump().equalsIgnoreCase("<=175"))
					&& (aciBeanData.getInternalVibration().equalsIgnoreCase("<=1.2"))) {
				ccpmax = cc * cw * (7.2 + ((785 * R) / (aciBeanData.getTemperatureee() + 17.8)));
			}
		}

		pressure2 = (aciBeanData.getDen() * 9.8 * aciBeanData.getVerticalformheight());
		pressure2 = pressure2 / 1000.00;

		if (flag) {
			if ((ccpmax > (30 * cw)) && (ccpmax < pressure2)) {
				aciBeanData.setPmax(ccpmax);
				int aciID = new ACIDAO().createRecordACI(aciBeanData);
				if (aciID > 0) {
					return new ACIResponse(aciID, aciBeanData.getTemperatureee(), Double.valueOf(df.format(ccpmax)));
				} else {
					throw new ACIException("Data's not saved to Database");
				}
			} else if (ccpmax < (30 * cw)) {
				ccpmax = 30 * cw;
				aciBeanData.setPmax(ccpmax);
				int aciID = new ACIDAO().createRecordACI(aciBeanData);
				if (aciID > 0) {
					return new ACIResponse(aciID, aciBeanData.getTemperatureee(), Double.valueOf(df.format(ccpmax)));
				} else {
					throw new ACIException("Data's not saved to Database");
				}
			} else {
				ccpmax = pressure2;
				aciBeanData.setPmax(ccpmax);
				int aciID = new ACIDAO().createRecordACI(aciBeanData);
				if (aciID > 0) {
					return new ACIResponse(aciID, aciBeanData.getTemperatureee(), Double.valueOf(df.format(ccpmax)));
				} else {
					throw new ACIException("Data's not saved to Database");
				}
			}
		}
		throw new ACIException("Something went wrong");
	}

	public int getTemperatureFromAPI(String lat, String lon) {
		String apikey = "b489d6cf97230d699a36c1b850e5e84f";
		RestTemplate restTemplate = new RestTemplate();
		final String url = "https://api.openweathermap.org/data/2.5/weather?lat=" + lat + "&lon=" + lon + "&APPID="
				+ apikey + "&units=metric";
		ResponseEntity<OpenWeatherResponse> response = restTemplate.getForEntity(url, OpenWeatherResponse.class);
		final int temp = response.getBody().getMain().getTemp();
		if (temp < 0)
			return 0;
		else
			return temp;
	}

	public boolean validateInput(String[] list, String input) {
		boolean found = true;
		for (String ip : list) {
			if (ip.equals(input)) {
				found = false;
				break;
			}
		}
		return found;
	}

	public boolean ACIIDinDatabase(int ACIID) throws ACIException {
		return new ACIDAO().ACIidinDB(ACIID);
	}

	public boolean ACIIDBelongsToUser(int ACIID, int userID) throws ACIException {
		return new ACIDAO().ACIIDforUser(ACIID, userID);
	}

	public List<ACIBean> getallACIDetailsOfAUser(ACIBean acidetailsuser) throws ACIException, JobDetailsException {
		if (acidetailsuser.getUserId() < 0) {
			throw new ACIException("User ID is Null");
		} else if (new JobDetailService().isUserinDatabase(acidetailsuser.getUserId())) {
			throw new ACIException("User ID is not in Database");
		}
		return new ACIDAO().getallACIDetailsfromDB(acidetailsuser);
	}

	/*public List<ACIBean> getACIDetailbyID(ACIBean acidetailsid) throws ACIException {

		if (acidetailsid.getAciID() < 0) {
			throw new ACIException("ACI ID is Null");
		} else if (ACIIDinDatabase(acidetailsid.getAciID())) {
			throw new ACIException("ACI  is not in Database");
		} else if (ACIIDBelongsToUser(acidetailsid.getAciID(), acidetailsid.getUserId())) {
			throw new ACIException("ACI not mapped to USER");
		}

		return new ACIDAO().getACIDetailfromDB(acidetailsid);
	}*/

}
