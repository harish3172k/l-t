package com.landt.rmkec.service;

import java.util.List;

import com.landt.rmkec.beans.JobDetailsBean;
import com.landt.rmkec.beans.JobDetailsUserIDBean;
import com.landt.rmkec.dao.JobDetailsDAO;
import com.landt.rmkec.exception.JobDetailsException;

public class JobDetailService {

	public int saveJobDetails(JobDetailsBean jobdetails) throws JobDetailsException {
		if (jobdetails.getCluster() == null) {
			throw new JobDetailsException("Cluster Doesnt have any value");
		} else if (jobdetails.getDuration() == 0) {
			throw new JobDetailsException("Duration is required");
		} else if (jobdetails.getJobLocation() == null || jobdetails.getJobLocation().equals("")) {
			throw new JobDetailsException("Location is required");
		} else if (jobdetails.getJobName() == null || jobdetails.getJobName().equals("")) {
			throw new JobDetailsException("Job Details is required");
		} else if (jobdetails.getNameofElement() == null || jobdetails.getNameofElement().equals("")) {
			throw new JobDetailsException("Element Name is required");
		} else if (jobdetails.getNameOfStructure() == null || jobdetails.getNameOfStructure().equals("")) {
			throw new JobDetailsException("Name of the Structure is required");
		} else if (jobdetails.getUserID() < 0) {
			throw new JobDetailsException("User ID is required");
		} else if (isUserinDatabase(jobdetails.getUserID())) {
			throw new JobDetailsException("User ID is not in Database");
		}

		return new JobDetailsDAO().StoreJobDetails(jobdetails);
	}

	public List<JobDetailsBean> getJobDetailsOfAUser(JobDetailsUserIDBean jobdetailsuser) throws JobDetailsException {
		if (jobdetailsuser.getUserID() < 0) {
			throw new JobDetailsException("User ID is Null");
		} else if (isUserinDatabase(jobdetailsuser.getUserID())) {
			throw new JobDetailsException("User ID is not in Database");
		}
		return new JobDetailsDAO().getJobDetailsfromDB(jobdetailsuser);
	}

	public boolean isUserinDatabase(int UserID) throws JobDetailsException {
		return new JobDetailsDAO().UserinDB(UserID);
	}

	public boolean isJobinDatabase(int JobID) throws JobDetailsException {
		return new JobDetailsDAO().JobinDB(JobID);
	}
}
