package com.landt.rmkec.service;

import java.text.DecimalFormat;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import com.landt.rmkec.beans.ACIBean;
import com.landt.rmkec.beans.CIRIABean;
import com.landt.rmkec.dao.ACIDAO;
import com.landt.rmkec.dao.CIRIADAO;
import com.landt.rmkec.exception.ACIException;
import com.landt.rmkec.exception.CIRIAException;
import com.landt.rmkec.exception.JobDetailsException;
import com.landt.rmkec.exception.TemperatureNotFoundException;
import com.landt.rmkec.response.CIRIAResponse;
import com.landt.rmkec.response.OpenWeatherResponse;

public class CIRIAService {
	DecimalFormat df = new DecimalFormat("#.##");

	public CIRIAResponse CIRIAServiceCalculate(CIRIABean ciriaBeanData)
			throws CIRIAException, TemperatureNotFoundException, JobDetailsException {
		if (ciriaBeanData.getUserId() < 0) {
			throw new CIRIAException("User ID cannot be null");
		} else if (new JobDetailService().isUserinDatabase(ciriaBeanData.getUserId())) {
			throw new CIRIAException("User ID is not in Database");
		} else if (ciriaBeanData.getJobID() < 0) {
			throw new CIRIAException("Job ID cannot be null");
		} else if (new JobDetailService().isJobinDatabase(ciriaBeanData.getJobID())) {
			throw new CIRIAException("Job ID is not in Database");
		} else if (!String.valueOf(ciriaBeanData.getTemperature()).matches("[0-9]{1,13}(\\.[0-9]*)?")) {
			throw new CIRIAException("Temperature is incorrect Format. Ex:00.00");
		} else if (!String.valueOf(ciriaBeanData.getVerticalFormHeight()).matches("[0-9]{1,13}(\\.[0-9]*)?")) {
			throw new CIRIAException("Vertical Form Height incorrect Format. Ex:00.00");
		} else if (!String.valueOf(ciriaBeanData.getVerticalPourHeight()).matches("[0-9]{1,13}(\\.[0-9]*)?")) {
			throw new CIRIAException("Vertical Pour Height incorrect Format. Ex:00.00");
		} else if (!String.valueOf(ciriaBeanData.getVolume()).matches("[0-9]{1,13}(\\.[0-9]*)?")) {
			throw new CIRIAException("Volume format is incorrect. Ex:00.00");
		} else if (!String.valueOf(ciriaBeanData.getRate()).matches("[0-9]{1,13}(\\.[0-9]*)?")) {
			throw new CIRIAException("Rate of Pouring incorrect Format. Ex:00.00");
		} else if (!String.valueOf(ciriaBeanData.getDensity()).matches("[0-9]{1,13}(\\.[0-9]*)?")) {
			throw new CIRIAException("Density incorrect Format. Ex:00.00");
		} else if (!String.valueOf(ciriaBeanData.getElementt()).matches("[0-9]{1,13}(\\.[0-9]*)?")) {
			throw new CIRIAException("Element incorrect Format. Ex:00.00");
		} /*
			 * else if (isValidLAT(String.valueOf(ciriaBeanData.getLatitude()))) { throw new
			 * CIRIAException("Latitude incorrect Format"); } else if
			 * (isValidLONG(String.valueOf(ciriaBeanData.getLongtitude()))) { throw new
			 * CIRIAException("Longtitude incorrect Format"); }
			 */
		double c1 = ciriaBeanData.getElementt();
		double k1 = ciriaBeanData.getTemperature() + 16;
		double K2 = (36 / k1);
		double K = K2 * K2;
		double pmax1 = ciriaBeanData.getDensity() * ciriaBeanData.getVerticalPourHeight();
		double pmax2;
		double R =ciriaBeanData.getRate();
		double pmax2_1 = ciriaBeanData.getVerticalFormHeight();
		double pmax2_2 = Math.sqrt(R);
		double pmax2_8 = c1 * pmax2_2;
		double pmax2_3 = pmax2_1 - pmax2_8;
		double pmax2_7 = Math.sqrt(pmax2_3);
		double pmax2_4 = pmax2_7 * K * ciriaBeanData.getC2();
		double pmax2_5 = c1 * Math.sqrt(R);
		double pmax2_6 = pmax2_4 + pmax2_5;
		if (ciriaBeanData.getVerticalFormHeight() > c1 * Math.sqrt(R)) {
			pmax2 = ciriaBeanData.getDensity() * pmax2_6;
			/// pmax2=pmax2_8;
		} else {
			pmax2 = pmax1;
		}
		double pmax = 0;
		if (pmax1 >= pmax2) {
			pmax = pmax2;
		} else {
			pmax = pmax1;
		}
		ciriaBeanData.setPmax(pmax);
		int ciriaID = new CIRIADAO().createRecordCIRIA(ciriaBeanData);
		if (ciriaID > 0) {
			return new CIRIAResponse(ciriaID, ciriaBeanData.getTemperature(), Double.valueOf(df.format(pmax)));
		} else {
			throw new CIRIAException("Data not saved in Database");
		}
	}

	public int getTemperatureFromAPI_forCIRIA(String lat, String lon) {
		String apikey = "b489d6cf97230d699a36c1b850e5e84f";
		RestTemplate restTemplate = new RestTemplate();
		final String url = "https://api.openweathermap.org/data/2.5/weather?lat=" + lat + "&lon=" + lon + "&APPID="
				+ apikey + "&units=metric";
		ResponseEntity<OpenWeatherResponse> response = restTemplate.getForEntity(url, OpenWeatherResponse.class);
		final int temp = response.getBody().getMain().getTemp();
		if (temp < 0)
			return 0;
		else
			return temp;
	}


	public boolean CIRIAIDinDatabase(int CIRIAID) throws CIRIAException {
		return new CIRIADAO().CIRIAidinDB(CIRIAID);
	}

	public boolean CIRIAIDBelongsToUser(int CIRIAID, int userID) throws CIRIAException {
		return new CIRIADAO().CIRIAIDforUser(CIRIAID, userID);
	}

	public List<CIRIABean> getallCIRIADetailsOfAUser(CIRIABean ciriadetailsuser)
			throws CIRIAException, JobDetailsException {
		if (ciriadetailsuser.getUserId() < 0) {
			throw new CIRIAException("User ID is Null");
		} else if (new JobDetailService().isUserinDatabase(ciriadetailsuser.getUserId())) {
			throw new CIRIAException("User ID is not in Database");
		}
		return new CIRIADAO().getallCIRIADetailsfromDB(ciriadetailsuser);
	}

	/*public List<CIRIABean> getCIRIADetailbyID(CIRIABean ciriaidetailsuser) throws CIRIAException {
		if (ciriaidetailsuser.getCiriaID() < 0) {
			throw new CIRIAException("ACI ID is Null");
		} else if (CIRIAIDinDatabase(ciriaidetailsuser.getCiriaID())) {
			throw new CIRIAException("ACI  is not in Database");
		} else if (CIRIAIDBelongsToUser(ciriaidetailsuser.getCiriaID(), ciriaidetailsuser.getUserId())) {
			throw new CIRIAException("ACI not mapped to USER");
		}
		return new CIRIADAO().getACIDetailfromDB(ciriaidetailsuser);
	}*/
}
