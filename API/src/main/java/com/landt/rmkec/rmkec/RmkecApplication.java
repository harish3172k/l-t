package com.landt.rmkec.rmkec;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

import com.landt.rmkec.controller.EmployeeController;
import com.landt.rmkec.controller.RESTAPIController;

@SpringBootApplication
@ComponentScan(basePackageClasses = RESTAPIController.class)
@ComponentScan(basePackageClasses = EmployeeController.class)
public class RmkecApplication {

	public static void main(String[] args) {
		SpringApplication.run(RmkecApplication.class, args);
	}
}
